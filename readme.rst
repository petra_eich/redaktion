###################
Redaktion
###################

*******************
Beschreibung
*******************


*******************
Codeignter Version 
*******************
3.1.5 (in Path-to-App\system\core\CodeIgniter.php)

*******************
Server Requirements
*******************
PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

*******
License
*******
Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

*******************
 Installation
*******************
-  git clone git clone git@bitbucket.org:petra_eich/redaktion.git
   ACHTUNG: aktuelle composer version!!!
  (evt. composer.lock löschen)
-  composer self-update
-  composer install
-  composer update
-  Datenbank erstellen und sql ausführen 
-  dist-Dateien umbenennen (.dist entfernen) und Zugangsdaten anpassen 