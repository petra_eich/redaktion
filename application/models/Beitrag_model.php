<?php

/**
 * Model Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * this class extends MY_Model
 *
 */
class Beitrag_model extends MY_Model
{

    /** @var    array   set column field database for datatable orderable */
    var $column_order = array(
        'beitraege.titel',
        'beitraege.datum',
        null,
        'statusname',
        'autorname',
        'beitraege.created',
    );

    /** @var    array   set column field database for datatable searchable just name is searchable */
    var $column_search = array(
        'beitraege.titel',
        'stati.name',
        'users.name',
    );

    /** @var array default order */
    var $order = array(
        'beitraege.titel' => 'asc',
    );

    /**
     * Class constructor
     * sets $table variable
     * 
     * @return viod 
     */
    function __construct()
    {

        $this->load->database();
        parent::__construct();
        $this->table = 'beitraege';
    }

    /**
     * 
     * @return boolean|array
     */
    public function get_latestnews()
    {

        $this->db->select('
            beitraege.titel, 
            beitraege.meldung,
            beitraege.created
        ');
        $this->db->from($this->table);
        $this->db->order_by('beitraege.created', 'desc');
        $this->db->limit(3);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        }
        return $query->result_array();
    }


    public function get_beitraege()
    {

        $this->db->select('
            beitraege.id, 
            beitraege.titel,  
            beitraege.status_id, 
            beitraege.autor_id, 
            beitraege.datum,
            beitraege.created,  
            stati.name as statusname,
            users.name as autorname
        ');
        $this->db->from($this->table);
        $this->db->join('stati', 'stati.id = beitraege.status_id');
        $this->db->join('users', 'users.id = beitraege.autor_id', 'left');
        $query = $this->db->get();

        $beitraege = $query->result_array();
        $i = 0;

        foreach ($beitraege as $beitrag) {

            $beitragID = $beitrag['id'];
            $beitraege[$i]['kategorien'] = $this->beitragkategorie_model->get_kategorien($beitragID);
            $i++;
        }
        return $beitraege;
    }


    public function _get_datatables_query()
    {

        $this->db->select('
            beitraege.id, 
            beitraege.titel, 
            beitraege.status_id, 
            beitraege.autor_id, 
            beitraege.datum,
            beitraege.created,  
            stati.name as statusname,
            users.name as autorname
        ');
        $this->db->from($this->table);
        $this->db->join('stati', 'stati.id = beitraege.status_id');
        $this->db->join('users', 'users.id = beitraege.autor_id', 'left');

        if ($this->role == 'autor') {
            $this->db->where(array('beitraege.autor_id' => $this->userId));
        }

        $i = 0;

        // loop column
        foreach ($this->column_search as $item) {

            // if datatable send POST for search
            if ($_POST['search']['value']) {

                // first loop
                if ($i === 0) {

                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {

                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //last loop
                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end(); //close bracket
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {

            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {

            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    /**
     * where conditon based on custom selectboxes
     * 
     * @param string    $datasource
     * @param array     $customfield
     */
    function _get_custom_field($datasource, $customfield)
    {
        
        // suchfeld (name/column)  
        $col = $customfield['col'];
        $cfname = $customfield['name'];

        if ($datasource = 'datatables') {

            if ($cfname == 'datum') {

                if (isset($_POST['columns'][$col]['search']['value']) && $_POST['columns'][$col]['search']['value'] != '') {

                    $this->db->where('beitraege.datum', date('Y-m-d', strtotime($_POST['columns'][$col]['search']['value'])));
                }
            } else {

                if (isset($_POST['columns'][$col]['search']['value']) && $_POST['columns'][$col]['search']['value'] != '') {

                    switch ($cfname) {

                        case 'stati':
                            $this->db->where('stati.name', $_POST['columns'][$col]['search']['value']);
                            break;

                        // case 'kategorien':
                        //     $this->db->where('kategorien.name', $_POST['columns'][$col]['search']['value']);
                        //     break;
                    }
                }
            }
        }
    }


    /**
     * 
     * @uses    Beitrag_model::_get_datatables_query()
     * @param   array   $custom_fields
     * @return  object   result
     */
    function get_datatables($custom_fields)
    {

        $this->_get_datatables_query($custom_fields);
        
        // für jedes Suchfeld (name/column)                
        foreach ($custom_fields as $cf) {
            $this->_get_custom_field('datatables', $cf);
        }

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        $beitraege = $query->result_array();

        $i = 0;

        foreach ($beitraege as $beitrag) {

            $beitragID = $beitrag['id'];
            $beitraege[$i]['kategorien'] = $this->beitragkategorie_model->get_kategorien($beitragID);
            $i++;
        }
        return $beitraege;
    }


    /**
     * Ermmittlung der Anzahl DS gefiltert
     * 
     * verwendet in: 
     *  controller -> beitraege/ajax_list
     * 
     * @param   string  $datasource
     * @param   array   $custom_fields
     * @return  integer
     */
    function count_filtered($datasource, $custom_fields)
    {

        switch ($datasource) {

            case 'datatables':
                $this->_get_datatables_query();
                break;
        }

        foreach ($custom_fields as $cf) {
            $this->_get_custom_field($datasource, $cf);
        }
        $query = $this->db->get();

        return $query->num_rows();
    }

    /**
     * 
     * @param   integer     $userId
     * @return  integer
     */
    public function get_by_userid($userId)
    {

        $this->db->select('id');
        $this->db->where('autor_id', $userId);
        $this->db->from($this->table);
        $query = $this->db->get();

        return $query->num_rows();
    }

}
