<?php

/**
 * Model Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * this class extends MY_Model
 */
class Status_model extends MY_Model
{

    /** @var    array   set column field database for datatable orderable */
    var $column_order = array(
        'stati.name',
        'stati.sequence',
        'stati.active',
        null,
    );

    /** @var    array   set column field database for datatable searchable just name is searchable */
    var $column_search = array(
        'name',
        'stati.sequence',
        'stati.active',
    );

    /** @var array default order */
    var $order = array(
        'stati.name' => 'asc',
    );

    /**
     * Class constructor
     * sets $table variable
     * 
     * @return viod 
     */
    function __construct()
    {

        $this->load->database();
        parent::__construct();
        $this->table = 'stati';
    }

    /**
     * 
     * @return boolean|array
     */
    public function get_stati()
    {

        $this->db->select('
            stati.name, 
            stati.id,
            stati.sequence,
            stati.active,
            beitraege.id AS beitrag
        ');
        $this->db->from($this->table);
        $this->db->order_by('name', 'desc');
        $this->db->group_by('stati.id');
        $this->db->join('beitraege', 'beitraege.status_id = stati.id', 'left');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        }
        return $query->result_array();
    }

    /**
     * 
     */
    public function _get_datatables_query()
    {

        $this->db->select('
            stati.name, 
            stati.id,
            stati.sequence,
            stati.active,
            beitraege.id AS beitrag
        ');
        $this->db->from($this->table);
        $this->db->group_by('stati.id');
        $this->db->join('beitraege', 'beitraege.status_id = stati.id', 'left');

        $i = 0;

        // loop column
        foreach ($this->column_search as $item) {

            // if datatable send POST for search
            if ($_POST['search']['value']) {

                // first loop
                if ($i === 0) {

                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {

                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //last loop
                if (count($this->column_search) - 1 == $i) :
                    $this->db->group_end(); //close bracket
                endif;
            }
            $i++;
        }

        if (isset($_POST['order'])) {

            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {

            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    /**
     * get query string 
     * 
     * @uses    Status_model::_get_datatables_query()
     * @return  object  result
     */
    function get_datatables()
    {

        $this->_get_datatables_query();

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

}
