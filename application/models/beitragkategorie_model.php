<?php
/**
 * Model Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * this class extends MY_Model
 */
class Beitragkategorie_model extends MY_Model
{
    /**
     * Class constructor
     * sets $table variable
     * 
     * @return viod 
     */
    function __construct()
    {

        $this->load->database();
        parent::__construct();
        $this->table = 'beitragkategorien';
    }


    /**
     * get kategorien and related users and beitraege
     * 
     * @return boolean|array 
     */
    public function get_kategorien($beitragID)
    {
        $this->db->select('
            kategorien.name, 
        ');
        $this->db->from($this->table);
        $this->db->where(array('beitrag_id' => $beitragID));
        $this->db->order_by('kategorien.sequence', 'desc');
        $this->db->join('kategorien', 'kategorien.id = beitragkategorien.kategorie_id', 'left');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return '';
        }
        $kategorien = $query->result_array();
        foreach ( $kategorien as $kategorie ) {
            $erg['kategorien'][] = $kategorie['name'];
        } 
        return implode( ', ', $erg['kategorien']);
    }

}
