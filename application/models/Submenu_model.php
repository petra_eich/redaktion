<?php

/**
 * Model Class
 * 
 * @author	eichp <petra.eich@web.de>
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * this class extends MY_Model
 *
 */

class Submenu_model extends MY_Model
{

    /**
     * Class constructor
     * sets $table variable
     * 
     * @return viod 
     */
    function __construct()
    {

        $this->load->database();
        parent::__construct();
        $this->table = 'submenus';
    }

    /**
     * used-by  MY_Cntroller::__construct
     * @return  object result
     */
    public function get_menunamen()
    {

        $this->db->select('            
            menus.name AS menu,  
            submenus.name AS submenu,  
        ');
        $this->db->from($this->table);

        $this->db->where('submenus.active', 1);
        $this->db->join('menus', 'menus.id = submenus.menu_id', 'left');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        }
        return $query->result_array();
    }

}
