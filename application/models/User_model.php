<?php

/**
 * Model Class
 * 
 * @author	eichp <petra_eich@7web.de>
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * this class extends MY_Model
 *
 */
class User_model extends MY_Model
{

    /** @var    array   set column field database for datatable orderable */
    var $column_order = array(
        'users.name',
        'users.username',
        'users.email',
        'users.role',
        null,
    );

    /** @var    array   set column field database for datatable searchable just name is searchable */
    var $column_search = array(
        'users.name',
        'users.username',
        'users.email',
        'users.role',
    );

    /** @var array default order */
    var $order = array(
        'users.name' => 'asc',
    );

    /**
     * Class constructor
     * sets $table variable
     * 
     * @return viod 
     */
    function __construct()
    {

        $this->load->database();
        parent::__construct();
        $this->table = 'users';
    }

    /**
     * 
     * @param   integer $id
     * @return  array|boolean
     */
    public function get_users($id = false)
    {

        $this->db->select('
            users.id, 
            users.name, 
            users.username,   
            users.email, 
            users.role, 
            users.created
        ');
        $this->db->order_by('users.name', 'desc');
        $this->db->group_by('users.id');
        $this->db->from($this->table);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        }
        $result = $query->result_array();

        $i = 0;

        foreach ($result as $value) {

            $userid = $value['id'];
            $result[$i]['beitrag'] = $this->beitrag_model->get_by_userid($userid);
            $i++;
        }
        return $result;
    }


    public function _get_datatables_query()
    {

        $this->db->select('
            users.id, 
            users.name, 
            users.username,  
            users.email, 
            users.role, 
            users.created
        ');
        $this->db->group_by('users.id');
        $this->db->from($this->table);

        $i = 0;

        // loop column
        foreach ($this->column_search as $item) {

            // if datatable send POST for search
            if ($_POST['search']['value']) {

                // first loop
                if ($i === 0) {

                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {

                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //last loop
                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end(); //close bracket
                }
            }
            $i++;
        }

        // here order processing
        if (isset($_POST['order'])) {

            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {

            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    /**
     * get query string 
     * 
     * @uses    User_model::_get_datatables_query()
     * @return  object  result
     */
    function get_datatables()
    {

        $this->_get_datatables_query();

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }


    public function get_by_username($name, $id = 0)
    {

        if ($id > 0) {
            $where['id !='] = $id;
        }
        $where['username'] = $name;
        $this->db->from($this->table);
        $this->db->where($where);

        return $this->db->count_all_results();
    }


    public function get_by_email($email, $id = 0)
    {

        if ($id > 0) {
            $where['id !='] = $id;
        }
        $where['email'] = $email;
        $this->db->from($this->table);
        $this->db->where($where);

        return $this->db->count_all_results();
    }


    public function register()
    {
        
        // encrypt password  
        $enc_pwd = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

        // users data array  
        $insert_data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => $enc_pwd,
            'username' => $this->input->post('username'),
        );
        return $this->db->insert($this->table, $insert_data);
    }


    public function login()
    {
        
        // encrypt password
        $pwd = $this->input->post('password');

        $this->db->where('username', $this->input->post('username'));
        $result = $this->db->get($this->table);

        if ($result->num_rows() == 1) {

            $enc_pdw = $result->row(0)->password;

            if (password_verify($pwd, $enc_pdw)) {
                return $result->row();
            } else {
                return false;
            }
        } else {

            return false;
        }
    }


    public function get_autorenlist()
    {

        $this->db->select('
            users.id AS user_id, 
            users.name,
            users.username
        ');
        $this->db->where('role', 'autor');
        $this->db->order_by('users.name', 'asc');
        $this->db->from($this->table);
        $query = $this->db->get();

        return $query->result_array();
    }

}
