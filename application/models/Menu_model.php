<?php

/**
 * Model Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * this class extends MY_Model
 *
 */

class Menu_model extends MY_Model
{

    /**
     * Class constructor
     * sets $table variable
     * 
     * @return viod 
     */
    function __construct()
    {

        $this->load->database();
        parent::__construct();
        $this->table = 'menus';
    }

    /**
     * used-by  MY_Cntroller::__construct
     * @return  array
     */
    public function get_menulist($lang)
    {


        if ($lang == 'german') {

            $menus = $this->menu_model->my_get_list('id AS menu_id, name, section, menu_url', array('active' => 1, 'lang' => 'de'), array('sequence' => 'desc', 'name' => 'desc'));
        } else {
            $menus = $this->menu_model->my_get_list('id AS menu_id, name, section, menu_url', array('active' => 1, 'lang' => 'en'), array('sequence' => 'desc', 'name' => 'desc'));
        }
        $i = 0;

        foreach ($menus as $menu) {

            $menus[$i]['submenu'] = $this->submenu_model->my_get_list('name, submenu_url', array('active' => 1, 'menu_id' => $menu['menu_id']), array('sequence' => 'desc'));
            $i++;
        }
        return $menus;
    }

}
