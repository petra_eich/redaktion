<?php
/**
 * Controller Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Kunden Controller Class 
 * -> this class extends MY_Controller
 */
class Stati extends MY_Controller
{
    /**
     * Class constructor
     * sets $title variable
     * 
     * @return	void
     */
    public function __construct()
    {
        parent::__construct();
        $this->title = 'Bearbeitungsstatus';
    }


    /**
     *  loads view stati/index
     */
    public function index()
    {
        if ($this->login == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Bitte erneut anmelden',
                'class' => 'danger',
            ));
            redirect('users/login');
        }

        if ($this->_check_permission(__FUNCTION__) == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Keine Berechtigung',
                'class' => 'danger',
            ));
            redirect('');
        }

        if ($this->status_model->my_count_all() == 0) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Es wurden noch keine Bearbeitungsstati angelegt.',
                'class' => 'danger',
            ));
        }

        $headerdata['title'] = $this->title;
        $headerdata['css'] = array_merge($this->css, $this->css_datatables);

        if (ENVIRONMENT == 'development') {
            $data['stati'] = $this->status_model->get_stati();
        }
        $data['title'] = $this->title;

        $footerdata['js'] = array_merge($this->js, $this->js_datatables);
        $footerdata['js'][] = base_url() . 'assets/lsmobil/js/statusindex.js?v=1';

        $this->load->view('templates/header', $headerdata);
        $this->load->view('stati/index', $data);
        $this->load->view('templates/footer', $footerdata);
    }


    /**
     * get data-list for view stati/index 
     *
     * @return	void
     */
    public function ajax_list()
    {
        $stati = $this->status_model->get_datatables();

        $data = [];
        $no = $_POST['start'];

        foreach ($stati as $status) {

            $no++;
            $row = [];
            $row[] = $status->name;
            $row[] = $status->sequence;

            if ($status->active == 1) {
                $active = '<input type="checkbox" disabled="disabled" value="' . $status->active . '" checked>';
            } else {
                $active = '<input type="checkbox" disabled="disabled" value="' . $status->active . '">';
            }
            $row[] = $active;
            $buttons = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Bearbeiten" onclick="edit_status(' . "'" . $status->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Bearbeiten</a> ';
            
            //add html for action
            if (!isset($status->beitrag)) {

                $buttons .= ' <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Löschen" onclick="delete_status(' . "'" . $status->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Löschen</a>';
            }
            $row[] = $buttons;
            $data[] = $row;
        }
        $output = array(
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->status_model->my_count_all(),
            'recordsFiltered' => $this->status_model->my_count_filtered('datatables'),
            'data' => $data,
        );   
        //output to json format
        echo json_encode($output);
    }


    /**
     * get date recordset (modal form / add action) 
     *
     * @return	void
     */
    public function ajax_showadd()
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        echo json_encode($data);
    }


    /**
     * get date recordset (modal form / update action) 
     *
     * @param   integer    $id
     */
    public function ajax_showupdate($id)
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {
            $data = $this->status_model->my_get_by_id($id);
        }
        echo json_encode($data);
    }


    /**
     * add new status 
     *
     * @return	void
     */
    public function ajax_add()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            $config = $this->config->item('status_add');
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $inputdata = array(
                    'name' => $this->input->post('name'),
                    'sequence' => $this->input->post('sequence'),
                    'active' => (int)($this->input->post('active')) == 1 ? 1 : 0,
                );
                $insert = $this->status_model->my_insert($inputdata);
                $data['success'] = true;
            } else {

                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function

                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }


    /**
     * update status 
     *
     * @return	void
     */
    public function ajax_update()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            if ($this->status_model->my_count_filter(array('name' => $this->input->post('name'), 'id !=' => $this->input->post('id')))) {

                $config = $this->config->item('status_add');
            } else {

                $config = $this->config->item('status_update');
            }
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $inputdata = array(
                    'name' => $this->input->post('name'),
                    'sequence' => $this->input->post('sequence'),
                    'active' => (int)($this->input->post('active')) == 1 ? 1 : 0,
                );
                $this->status_model->my_update($inputdata, array('id' => $this->input->post('id')));
                $data['success'] = true;
            } else {
            
                // validation errors as array
                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function
                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }


    /**
     * delete status 
     *
     * @param    integer    $id
     * @return	void
     */
    public function ajax_delete($id)
    {
        $data = array(
            'status' => true,
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {
            $this->status_model->my_delete_by_id($id);
        }
        echo json_encode($data);
    }

}
