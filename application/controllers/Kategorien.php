<?php

/**
 * Controller Class
 * 
 * @author	eichp <petra_eichweb.de>
 */
defined('BASEPATH') or exit('No direct script access allowed');


/**
 * Kategorien Controller Class 
 * -> this class extends MY_Controller
 *
 */
class Kategorien extends MY_Controller
{
    /**
     * Class constructor
     * sets $title variable
     * 
     * @return	void
     */
    public function __construct()
    {
        parent::__construct();
        $this->title = 'Kategorien';
    }


    /**
     *  loads view kategorien/index
     *
     */
    public function index()
    {
        if ($this->login == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Bitte erneut anmelden',
                'class' => 'danger',
            ));
            redirect('users/login');
        }

        if ($this->_check_permission(__FUNCTION__) == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Keine Berechtigung',
                'class' => 'danger',
            ));
            redirect('');
        }

        if ($this->kategorie_model->my_count_all() == 0) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Es wurden noch keine Kategorien angelegt.',
                'class' => 'danger',
            ));
        }

        $headerdata['title'] = $this->title;
        $headerdata['css'] = array_merge($this->css, $this->css_datatables);

        $data['kategorien'] = array();

        if (ENVIRONMENT == 'development') {
            $data['kategorien'] = $this->kategorie_model->get_kategorien();
        }
        $data['title'] = $this->title;

        $footerdata['js'] = array_merge($this->js, $this->js_datatables);
        $footerdata['js'][] = base_url() . 'assets/lsmobil/js/kategorieindex.js?v=2';

        $this->load->view('templates/header', $headerdata);
        $this->load->view('kategorien/index', $data);
        $this->load->view('templates/footer', $footerdata);
    }


    /**
     * get data-list for view kategorien/index 
     *
     * @return  void
     */
    public function ajax_list()
    {
        $kategorien = $this->kategorie_model->get_datatables();
        $data = [];
        $no = $_POST['start'];

        foreach ($kategorien as $kategorie) {

            $no++;
            $row = [];
            $row[] = $kategorie->name;
            $row[] = $kategorie->sequence;
            if ($kategorie->active == 1) {
                $active = '<input type="checkbox" disabled="disabled" value="' . $kategorie->active . '" checked>';
            } else {
                $active = '<input type="checkbox" disabled="disabled" value="' . $kategorie->active . '">';
            }
            $row[] = $active;
            $buttons = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Bearbeiten" onclick="edit_kategorie(' . "'" . $kategorie->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Bearbeiten</a> ';            
            //add html for action
            if (!isset($kategorie->beitrag)) {

                $buttons .= ' <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Löschen" onclick="delete_kategorie(' . "'" . $kategorie->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Löschen</a>';
            }
            $row[] = $buttons;
            $data[] = $row;
        }
        $output = array(
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->kategorie_model->my_count_all(),
            'recordsFiltered' => $this->kategorie_model->my_count_filtered('datatables'),
            'data' => $data,
        );
        echo json_encode($output);
    }


    /**
     * get date recordset (modal form / add action) 
     *
     * @return  void
     */
    public function ajax_showadd()
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        echo json_encode($data);
    }


    /**
     * get date recordset (modal form / update action) 
     *
     * @param type $id
     * @return  void
     */
    public function ajax_showupdate($id)
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {
            $data = $this->kategorie_model->my_get_by_id($id);
        }
        echo json_encode($data);
    }


    /**
     * add new kategorie 
     *
     * @return  void
     */
    public function ajax_add()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            $config = $this->config->item('kategorie_add');
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $inputdata = array(
                    'name' => $this->input->post('name'),
                    'sequence' => $this->input->post('sequence'),
                    'active' => (int)($this->input->post('active')) == 1 ? 1 : 0,
                );
                $insert = $this->kategorie_model->my_insert($inputdata);
                $data['success'] = true;
            } else {
            
                // validation errors as array
                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function
                    //if error is their for field then only add in $errors_array array
                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }


    /**
     * update kategorie 
     *
     * @return  void
     */
    public function ajax_update()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            if ($this->kategorie_model->my_count_filter(array('name' => $this->input->post('name'), 'id !=' => $this->input->post('id')))) {

                $config = $this->config->item('kategorie_add');
            } else {

                $config = $this->config->item('kategorie_update');
            }
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $inputdata = array(
                    'name' => $this->input->post('name'),
                    'sequence' => $this->input->post('sequence'),
                    'active' => (int)($this->input->post('active')) == 1 ? 1 : 0,
                );
                $this->kategorie_model->my_update($inputdata, array('id' => $this->input->post('id')));
                $data['success'] = true;
            } else {
            
                // validation errors as array
                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function
                    //if error is their for field then only add in $errors_array array
                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }


    /**
     * delete kategorie 
     *
     * @param    integer $id
     * @return   void
     */
    public function ajax_delete($id)
    {
        $data = array(
            'status' => true,
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {
            $this->kategorie_model->my_delete_by_id($id);
        }
        echo json_encode($data);
    }

}
