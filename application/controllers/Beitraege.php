<?php
/**
 * Controller Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Neuiglkeiten Controller Class 
 * -> this class extends MY_Controller
 */
class Beitraege extends MY_Controller
{
    /**
     * Class constructor
     * sets $title variable
     * 
     * @return	void
     */
    public function __construct()
    {
        parent::__construct();
        $this->title = 'Beiträge';
    }


    /**
     *  loads view beitraege/index
     *
     */
    public function index()
    {
        if ($this->login == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Bitte erneut anmelden',
                'class' => 'danger',
            ));
            redirect('users/login');
        }

        if ($this->_check_permission(__FUNCTION__) == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Keine Berechtigung',
                'class' => 'danger',
            ));
            redirect('');
        }

        if ($this->beitrag_model->my_count_all() == 0) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Es wurden noch keine Beiträge angelegt.',
                'class' => 'danger',
            ));
        }

        if ($this->role == 'autor' && $this->beitrag_model->my_count_filter(array('autor_id' => $this->userId)) == 0) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Es wurden noch keine Beiträge von Autor ' . $this->loginuser . ' angelegt.',
                'class' => 'danger',
            ));
        }

        $headerdata['title'] = $this->title;
        $headerdata['css'] = array_merge($this->css, $this->css_datatables, $this->css_select, $this->css_datepicker);

        $data['beitragskategorien'] = $this->kategorie_model->get_beitragskategorien(12);
        $data['kategorien'] = $this->kategorie_model->my_get_list('id, name', array('active' => 1), array('sequence' => 'desc'));
        $data['stati'] = $this->status_model->my_get_list('id, name', array('active' => 1), array('sequence' => 'desc'));
        $data['beitraege'] = array();

        if (ENVIRONMENT == 'development') {
            $data['beitraege'] = $this->beitrag_model->get_beitraege();
        }
        $data['title'] = $this->title;

        $footerdata['js'] = array_merge($this->js, $this->js_datatables, $this->js_select, $this->js_datepicker);
        $footerdata['js'][] = base_url() . 'assets/lsmobil/js/beitragindex.js?v=2';

        $this->load->view('templates/header', $headerdata);
        $this->load->view('beitraege/index', $data);
        $this->load->view('templates/footer', $footerdata);
    }


    /**
     * get data-list for view beitraege/index 
     *
     * @return  void
     */
    public function ajax_list()
    {
        $custom_fields = array(
            array(
                'name' => 'stati',
                'col' => 3,
            ),
            // array(
            //     'name' => 'kategorien',
            //     'col' => 2,
            // ),
            array(
                'name' => 'datum',
                'col' => 1,
            ),
        );
        $beitraege = $this->beitrag_model->get_datatables($custom_fields);

        $data = [];
        $no = $_POST['start'];

        foreach ($beitraege as $beitrag) {

            $no++;
            $row = [];
            $row[] = $beitrag['titel'];
            $row[] = date('d.m.Y', strtotime($beitrag['datum']));
            $row[] = strlen($beitrag['kategorien']) > 0 ? $beitrag['kategorien'] : '<div class="text-danger">keine Angabe</div>';
            $row[] = $beitrag['statusname'];
            $row[] = strlen($beitrag['autorname']) > 0 ? $beitrag['autorname'] : '<div class="text-danger">keine Angabe</div>';
            $row[] = date('d.m.Y H:i:s', strtotime($beitrag['created']));

            $buttons = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Bearbeiten" onclick="showupdate_beitrag(' . "'" . $beitrag['id'] . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Bearbeiten</a> ';
            
            //add html for action
            if ($this->admin) {
                $buttons .= ' <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Löschen" onclick="delete_beitrag(' . "'" . $beitrag['id'] . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Löschen</a>';
            }
            $row[] = $buttons;
            $data[] = $row;
        }
        $output = array(
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->beitrag_model->my_count_all(),
            'recordsFiltered' => $this->beitrag_model->count_filtered('datatables', $custom_fields),
            'data' => $data,
        );
        echo json_encode($output);
    }


    /**
     * get date recordset (modal form / add action) 
     *
     * @return  void
     */
    public function ajax_showadd()
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            $data['kategorien'] = $this->kategorie_model->my_get_list('id, name', array('active' => 1), array('sequence' => 'desc'));
            $data['stati'] = $this->status_model->my_get_list('id, name', array('active' => 1), array('sequence' => 'desc'));
        }
        echo json_encode($data);
    }


    /**
     * get date recordset (modal form / update action) 
     *
     * @return  void
     */
    public function ajax_showupdate($id)
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {

            $data['admin'] = $this->admin;
            $data['kategorien'] = $this->kategorie_model->get_beitragskategorien($id);
            $data['stati'] = $this->status_model->my_get_list('id, name', array('active' => 1), array('sequence' => 'desc'));
            $data['beitrag'] = $this->beitrag_model->my_get_by_id($id);
            $data['beitrag']['datum'] = date('d.m.Y', strtotime($data['beitrag']['datum']));
        }
        echo json_encode($data);
    }


    /**
     * add new beitrag 
     *
     * @return  void
     */
    public function ajax_add()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            $config = $this->config->item('beitrag_add');

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                // insert formadata in table beitraege
                $inputdata = array(
                    'titel' => $this->input->post('titel'),
                    'meldung' => $this->input->post('meldung'),
                    'status_id' => 1,
                    'datum' => date('Y-m-d', strtotime($this->input->post('datum'))),
                    'autor_id' => $this->role == 'autor' ? $this->userId : 0,
                );
                $beitrag_id = $this->beitrag_model->my_insert($inputdata);

                $kategorien = $this->kategorie_model->my_get_list('id', array('active' => 1), Null);
                foreach ($kategorien as $kategorie) {

                    if ((int)($this->input->post('check_' . $kategorie['id'])) == 1) {

                        $inputkategorien = array(
                            'beitrag_id' => $beitrag_id,
                            'kategorie_id' => $kategorie['id'],
                        );
                        $insert = $this->beitragkategorie_model->my_insert($inputkategorien);
                    }
                }
                $data['success'] = true;
            } else {

                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function

                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }

    /**
     * update Beitrag
     *
     * @return void
     */
    public function ajax_update()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            $config = $this->config->item('beitrag_add');
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $beitrag_id = $this->input->post('id');
                $inputdata = array(
                    'titel' => $this->input->post('titel'),
                    'meldung' => $this->input->post('meldung'),
                    'status_id' => $this->input->post('status_id'),
                    'datum' => date('Y-m-d', strtotime($this->input->post('datum'))),
                );
                $this->beitrag_model->my_update($inputdata, array('id' => $beitrag_id));

                $kategorien = $this->kategorie_model->my_get_list('id', array('active' => 1), Null);
                foreach ($kategorien as $kategorie) {

                    $beitragkategorie = $this->beitragkategorie_model->my_count_filter(array('kategorie_id' => $kategorie['id'], 'beitrag_id' => $beitrag_id));
                    $inputkategorien = array(
                        'beitrag_id' => $beitrag_id,
                        'kategorie_id' => $kategorie['id'],
                    );

                    if ((int)($this->input->post('check_' . $kategorie['id'])) == 1 && $beitragkategorie == 0) {

                        $insert = $this->beitragkategorie_model->my_insert($inputkategorien);
                    } elseif ((int)($this->input->post('check_' . $kategorie['id'])) == 0 && $beitragkategorie == 1) {
                        
                        $this->beitragkategorie_model->my_delete($inputkategorien);
                    }
                }                
                $data['success'] = true;
            } else {

                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function
                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }


    /**
     * delete beitrag 
     *
     * @return  void
     */
    public function ajax_delete($id)
    {
        $data = array(
            'status' => true,
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {
            // delete beitrag / beitragkategorien
            $this->beitrag_model->my_delete_by_id($id);
            $this->beitragkategorie_model->my_delete(array('beitrag_id' => $id));
        }
        echo json_encode($data);
    }

}
