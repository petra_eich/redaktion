<?php
/**
 * Controller Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Users Controller Class 
 * -> this class extends MY_Controller
 */
class Users extends MY_Controller
{
    /**
     * Class constructor
     * sets $title variable
     * 
     * @return	void
     */
    public function __construct()
    {
        parent::__construct();
        $this->title = 'Userverwaltung';
    }

    /**
     *
     */
    public function index()
    {
        if ($this->login == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Bitte erneut anmelden',
                'class' => 'danger',
            ));
            redirect('users/login');
        }

        if ($this->_check_permission(__FUNCTION__) == false) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Keine Berechtigung',
                'class' => 'danger',
            ));
            redirect('');
        }

        if ($this->user_model->my_count_all() == 0) {

            $this->session->set_flashdata('flash', array(
                'message' => 'Es wurden noch keine User angelegt.',
                'class' => 'danger',
            ));
        }
        $headerdata['title'] = $this->title;
        $headerdata['css'] = array_merge($this->css, $this->css_datatables, $this->css_select);

        $data['title'] = $this->title;
        $data['users'] = array();

        if (ENVIRONMENT == 'development') {
            $data['users'] = $this->user_model->get_users();
        }
        $footerdata['js'] = array_merge($this->js, $this->js_datatables, $this->js_select);
        $footerdata['js'][] = base_url() . 'assets/lsmobil/js/userindex.js?v=1';

        $this->load->view('templates/header', $headerdata);
        $this->load->view('users/index', $data);
        $this->load->view('templates/footer', $footerdata);
    }


    /**
     * get data-list for view users/index 
     *
     */
    public function ajax_list()
    {

        $users = $this->user_model->get_datatables();
        $data = [];
        $no = $_POST['start'];

        foreach ($users as $user) {

            $no++;
            $row = [];
            $row[] = $user->name;
            $row[] = $user->username;
            $row[] = $user->email;
            $row[] = $user->role;
            $row[] = date('d.m.Y H:i:s', strtotime($user->created));

            $buttons = '';

            switch ($user->role) {
                case 'admin':
                    if ($this->admin) {
                        $buttons = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Bearbeiten" onclick="edit_user(' . "'" . $user->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Bearbeiten</a> ';
                    }
                    break;

                case 'keyuser':
                    if ($this->admin || $this->role == 'keyuser') {
                        $buttons = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Bearbeiten" onclick="edit_user(' . "'" . $user->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Bearbeiten</a> ';
                    }
                    break;

                case 'autor':
                    if ($this->admin || $this->role == 'keyuser' || $this->role == 'autor') {
                        $buttons = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Bearbeiten" onclick="edit_user(' . "'" . $user->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Bearbeiten</a> ';
                    }
                    break;

                case 'gast':
                    if ($this->admin || $this->role == 'keyuser') {
                        $buttons = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Bearbeiten" onclick="edit_user(' . "'" . $user->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Bearbeiten</a> ';
                    }
                    break;
            }
                      
            //add html for action                       
            if ($this->admin) {

                $buttons .= ' <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Löschen" onclick="delete_user(' . "'" . $user->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Löschen</a>';
            }
            $row[] = $buttons;
            $data[] = $row;
        }
        $output = array(
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->user_model->my_count_all(),
            'recordsFiltered' => $this->user_model->my_count_filtered('datatables'),
            'data' => $data,
        );
        echo json_encode($output);
    }


    /**
     *
     */
    public function register()
    {
        $headerdata['title'] = 'registrieren';
        $headerdata['css'] = array_merge($this->css, $this->css_select);

        $data['title'] = 'Registrierung';

        $footerdata['js'] = array_merge($this->js, $this->js_select);
        $footerdata['js'][] = base_url() . 'assets/lsmobil/js/loginregister.js?v=1';

        if (isset($_POST['register'])) {

            $config = $this->config->item('user_register');
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() === false) {

                $this->session->set_flashdata('flash', array(
                    'message' => 'Bitte prüfe Deine Eingaben.',
                    'class' => 'danger',
                ));
            } else {

                // Passwortverschlüsselung
                $this->user_model->register();

                $this->session->set_flashdata('flash', array(
                    'message' => 'Du bist nun registriert. <br> Du erhältst Mail, sobald wir Dich freigeschaltet haben.',
                    'class' => 'success',
                ));
                redirect('home');
            }
        }
        $this->load->view('templates/header', $headerdata);
        $this->load->view('users/register', $data);
        $this->load->view('templates/footer', $footerdata);
    }


    /**
     *
     */
    public function login()
    {
        $headerdata['title'] = 'login';
        $headerdata['css'] = $this->css;

        $data['title'] = 'Login';

        $footerdata['js'] = $this->js;
        $footerdata['js'][] = base_url() . 'assets/lsmobil/js/loginregister.js?v=2';

        if (isset($_POST['login'])) {

            $config = $this->config->item('user_login');
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() === false) {

                $this->session->set_flashdata('flash', array(
                    'message' => 'Bitte prüfe Deine Eingaben.',
                    'class' => 'danger',
                ));
            } else {

                $row = $this->user_model->login();

                if ($row) {

                    // create session data
                    $user_data = array(
                        'user_id' => $row->id,
                        'username' => $row->username,
                        'role' => $row->role,
                        'logged_in' => true,
                    );
                    $this->session->set_userdata($user_data);

                    $this->session->set_flashdata('flash', array(
                        'message' => 'Du bist nun einloggt.',
                        'class' => 'success',
                    ));

                    switch ($row->role) {

                        case 'admin':
                            redirect('/');
                            break;

                        case 'autor':
                            redirect('beitraege');
                            break;

                        case 'keyuser':
                            redirect('/');
                            break;

                        default:
                            redirect('/');
                            break;
                    }
                } else {

                    $this->session->set_flashdata('flash', array(
                        'message' => 'Login fehlgeschlagen.',
                        'class' => 'danger',
                    ));
                }
            }
        }
        $this->load->view('templates/header', $headerdata);
        $this->load->view('users/login', $data);
        $this->load->view('templates/footer', $footerdata);
    }


    /**
     *
     */
    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('role');

        $this->session->set_flashdata('flash', array(
            'message' => 'Du hast Dich erfolgreich ausgeloggt.',
            'class' => 'success',
        ));
        redirect('users/login');
    }


    /**
     * get data recordset and depotlist(show modal form / update action) 
     *
     * @return  void
     */
    public function ajax_showupdate($id)
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {

            $data = $this->user_model->my_get_by_id($id);

            $data['roles'] = array(
                array(
                    'role' => 'gast',
                    'name' => 'gast',
                ),
                array(
                    'role' => 'keyuser',
                    'name' => 'keyuser',
                ),
                array(
                    'role' => 'autor',
                    'name' => 'autor',
                ),
            );
            if ($this->admin) {

                $data['roles'][] = array(
                    'role' => 'admin',
                    'name' => 'admin',
                );
            }
        }
        echo json_encode($data);
    }


    /**
     * get depots and usererroles (show modal form / add action) 
     *
     * @return  void
     */
    public function ajax_showadd()
    {
        $data = array(
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {

            $data['roles'] = array(
                array(
                    'role' => 'gast',
                    'name' => 'gast',
                ),
                array(
                    'role' => 'keyuser',
                    'name' => 'keyuser',
                ),
                array(
                    'role' => 'autor',
                    'name' => 'autor',
                ),
            );
            if ($this->admin) {

                $data['roles'][] = array(
                    'role' => 'admin',
                    'name' => 'admin',
                );
            }
        }
        echo json_encode($data);
    }


    /**
     * add new user
     *
     * @return  void
     */
    public function ajax_add()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            $config = $this->config->item('user_add');
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $enc_pwd = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

                $inputdata = array(
                    'name' => $this->input->post('name'),
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'password' => $enc_pwd,
                    'role' => $this->input->post('role'),
                );
                $insert = $this->user_model->my_insert($inputdata);
                $data['success'] = true;
            } else {

                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function
                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }


    /**
     * update user 
     *
     * @return  void
     */
    public function ajax_update()
    {
        $data = array(
            'success' => false,
            'messages' => array(),
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );

        if ($data['permission'] && $data['login']) {

            $config = $this->config->item('user_update');

            if ($this->user_model->get_by_username($this->input->post('username'), $this->input->post('id')) > 0) {

                $config[] = array(
                    'field' => 'username',
                    'label' => 'Loginname',
                    'rules' => array(
                        'is_unique[users.username]',
                    ),
                    'errors' => array(
                        'is_unique' => 'Dieser {field} existiert bereits',
                    ),
                );
            }

            if ($this->user_model->get_by_email($this->input->post('email'), $this->input->post('id')) > 0) {

                $config[] = array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => array(
                        'is_unique[users.email]',
                    ),
                    'errors' => array(
                        'is_unique' => 'Dieser {field} existiert bereits',
                    ),
                );
            }
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {

                $inputdata = array(
                    'name' => $this->input->post('name'),
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'role' => $this->input->post('role'),
                );

                if (strlen($this->input->post('password')) > 2) {

                    $enc_pwd = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                    $inputdata['password'] = $enc_pwd;
                }
                $this->user_model->my_update($inputdata, array('id' => $this->input->post('id')));
                $data['success'] = true;
            } else {

                foreach ($config as $row) {

                    $field = $row['field'];          //field name
                    $error = form_error($field);    //error for field name ->form_error() is inbuilt function
                    if ($error) {
                        $errors_array[$field] = $error;
                    }
                }
                $data['messages'] = $errors_array;
            }
        }
        echo json_encode($data);
    }


    /**
     * delete user
     *
     * @param integer   $id
     * @return  void
     */
    public function ajax_delete($id)
    {
        $data = array(
            'status' => true,
            'login' => $this->login,
            'permission' => $this->_check_permission(__FUNCTION__),
        );
        if ($data['permission'] && $data['login']) {

            $this->user_model->my_delete_by_id($id);
        }
        echo json_encode($data);
    }


    /**
     *
     * @param   string  $username
     * @return  boolean
     */
    function check_username_exists($username)
    {
        if ($this->user_model->check_username_exists($username)) {
            return true;
        } else {
            return false;
        }
    }

}
