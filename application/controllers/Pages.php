<?php
/**
 * Controller Class
 * 
 * @author	eichp <petra_eich@web.de>
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Pages Controller Class 
 * -> this class extends MY_Controller
 *
 */
class Pages extends MY_Controller
{
    /**
     * Class constructor
     * sets $title variable
     * 
     * @return	void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Undocumented function
     *
     * @param string $page
     * @return void
     */
    public function view($page = 'home')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            show_404();
        }

        $headerdata['title'] = ucfirst($page);
        $headerdata['css'] = $this->css;
        if ($page == 'home') {
            $data['beitraege'] = $this->beitrag_model->get_latestnews();
        }
        $data['title'] = ucfirst($page);

        $footerdata['js'] = $this->js;

        $this->load->view('templates/header', $headerdata);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $footerdata);
    }

}
