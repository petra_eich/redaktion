<h2><?= $title; ?></h2>
<div class="row">
  <div class="col-md-12">
        <h3>Redaktions_Tool</h3> 
        <p>Kleines Übungs-Tool zur dynmaischen Verwaltung von redaktionellen Texten. Umgesetzt mit dem PHP-Framework CodeIgniter.</p> 
        <p>Logindaten für die unterschiedlichen Benutzergruppen (loginname/pwd):</p> 
        <ul>
            <li>Adminstrative Berechtigung: admin/admin</li>
            <li>Keyuser Berechtigung: keyuser/keyuser</li>
            <li>Autor Berechtigung: autor/autor</li>
        </ul>
 
        <h3>Kontaktdaten</h3>
        Petra Eich <br>
        Essen <br>
        mail <a href="mailto:petra_eich@web.de">petra_eich@web.de</a><br>
        <a href="https://www.xing.com/profile/Petra_Eich4/cv">xing</a>
    </div>
</div>
