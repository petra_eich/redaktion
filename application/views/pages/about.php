<h2><?= $title; ?></h2>
<div class="row">
  <div class="col-md-4">
        <img src="<?= base_url() . 'petra.jpg'; ?>" alt="Petra Eich" class="img-circle img-responsive"> 
  </div>
  <div class="col-md-8">
        <h3>
            Hallo, <br> 
            mein Name ist Petra.
        </h3> 
        <p>Als leidenschaftliche PHP-Entwicklerin plane, entwickle und realisiere ich erfolgreich anspruchsvolle IT-Projekte.</p> 
        <p>Meine Kernkompetenzen lassen sich folgendermassen zusammenfassen:</p> 
        <ul>
            <li>Umsetzung von Projekten in PHP individuellen nach Anforderung</li>
            <li>Planung und Konzeption von Anwendungen aus verschiedensten Bereichen (E-Commerce, Controlling, Personalplanung, Logistik)</li>
            <li>Analyse und Weiterentwicklung von bestehenden Projekten</li>
            <li>Entwicklung von dynamischen Webseiten sowie einzelner Applikationen und Module</li>
            <li>Datenbankdesign und Optimierung der Datenbanken</li>
        </ul>
 
        <h3>Kontaktdaten</h3>
        Petra Eich <br>
        Essen <br>
        mail <a href="mailto:petra_eich@web.de">petra_eich@web.de</a><br>
        <a href="https://www.xing.com/profile/Petra_Eich4/cv">xing</a>
    </div>
</div>
