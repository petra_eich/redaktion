<h2><?= $title; ?></h2>
<p>Herzlich Willkommen</p>
<p>Dies ist das Readaktions-Tool Version 1.0</p> <?php 

if ( $beitraege ) { ?>

    <h3>Neue Meldungen</h3><?php

    foreach ( $beitraege as $neu ) {  ?>                   

        <p>
            <strong><?= $neu['titel']; ?></strong><br>
            <?= $neu['meldung']; ?><br>
            <small><?= date('d.m.Y H:i:s', strtotime($neu['created'])); ?></small>
        </p>
        <hr> <?php
    } 
} ?> 

<div class="row"> <?php 
    if ( ENVIRONMENT == 'development' ) {
        
        if ( $beitraege ) { ?>
            <pre><?php print_r($beitraege); ?></pre> <?php 
        }
    } ?>
</div>