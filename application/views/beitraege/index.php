    <?php 
    $modalname = 'beitraege'; ?>

    <h2><?= $title; ?></h2>
    
    <div class="row"> 
        <div class="col-md-12">
            <button class="btn btn-success" title="Beitrag anlegen" onclick="showadd_beitrag()">
                <i class="glyphicon glyphicon-plus"></i> Beitrag anlegen
            </button>
            
            <button class="btn btn-default" title="Reload" onclick="reload_table()">
                <i class="glyphicon glyphicon-refresh"></i> Reload
            </button>
        </div>
    </div>
    
    <hr>
    
    <div class="row form-inline">

        <div class="col-md-4">
            <span class="control-label">Bearbeitungsstatus</span>
            <select class="form-control" id="selectstatus" name="selectstatus">
                <option value="">alle</option> <?php
                foreach ($stati as $status) { ?>
                    <option value="<?= $status['name']; ?>"><?= $status['name']; ?></option> <?php
                } ?>
            </select>
        </div>
        
        <div class="col-md-4">
            <!-- <span class="control-label">Kategorie</span>
            <select class="form-control" id="selectkategorie" name="selectkategorie">
                <option value="">alle</option> <?php
                foreach ($kategorien as $kategorie) { ?>
                    <option value="<?= $kategorie['name']; ?>"><?= $kategorie['name']; ?></option> <?php
                } ?>
            </select> -->
        </div>    

        <div class="col-md-4">
            <div class="input-group date datepicker_format" id="datepickerselect">
                <input type="text" class="form-control" value="" name="selectdatum" id="selectdatum"> 
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>    
    
    
    <br>  
    
    <div class="row"> 
        <div class="col-md-12"> 
            <table id="<?= $modalname; ?>table" class="table table-striped table-hover table-condensed table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Titel</th>
                        <th>Datum</th>
                        <th class="nosort">Kategorie</th>
                        <th>Status</th>
                        <th>Autor</th>
                        <th>erstellt am</th>
                        <th class="nosort" style="min-width:200px;">Aktion</th>
                    </tr>
                </thead>

                <tbody></tbody>
                
                <tfoot>
                    <tr>
                        <th>Titel</th>
                        <th>Datum</th>
                        <th>Kategorie</th>
                        <th>Status</th>
                        <th>Autor</th>
                        <th>erstellt am</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>        
    </div>  
</div>

<!-- Bootstrap modal -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Beitrag Formular</h3>
                <small><?= $this->role == 'autor' ? 'Autor: ' . $this->loginuser : 'kein Autor'; ?></small>
            </div>
            
            <div class="modal-body form">
                
                <div id="<?= $modalname; ?>alerttext"></div>
                
                <form action="#" id="form" class="form-horizontal">
                    
                    <input type="hidden" value="" name="id"> 
                    
                    <div class="form-body"> 
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Titel</label>
                            
                            <div class="col-md-9">
                                <input name="titel" id="titel" value="" placeholder="Beitrag" value="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Meldung</label>
                            
                            <div class="col-md-9"><?php
                                $data = array(
                                    'name' => 'meldung',
                                    'id' => 'meldung',
                                    'type' => 'text',
                                    'value' => '',
                                    'class' => 'form-control',
                                    'placeholder' => 'Meldung',
                                );
                                echo form_textarea($data); ?>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Datum</label>
                            
                            <div class="col-md-9">
                                <div class="input-group date datepicker_format" id="datepicker">
                                    <input type="text" class="form-control datepicker" name="datum" id="inputDatum"> 
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3" for="checkkategorie">Kategorie</label>
                            <div class="col-md-9"> <?php 
                                foreach ($kategorien as $kategorie) { ?>
                                
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" name="check_<?= $kategorie['id']; ?>" id="check_<?= $kategorie['id']; ?>" value="0"><?= $kategorie['name']; ?><label>
                                    </div> <?php
                                } ?>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group statushidden">
                            <label class="control-label col-md-3" for="status_id">Bearbeitungsstatus</label>
                            
                            <div class="col-md-9"> 
                                <select class="selectpicker" name="status_id" id="status_id"> 
                                    <option value="">status</option> 
                                </select>  
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" id="btnSave" title="Speichern" onclick="save()" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i> Speichern
                </button>
                
                <button type="button" class="btn btn-danger" title="Abbrechen" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i> Abbrechen
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row"> <?php 
    if (ENVIRONMENT == 'development') {
        if ($beitraege) { ?>
            <pre><?php print_r($beitraege); ?></pre> <?php 
        }
        if ($beitragskategorien) { ?>
            <pre><?php print_r($beitragskategorien); ?></pre> <?php 
        }
    } ?>
</div>