<?= form_open('users/register'); ?>
    <div class="row">
        <div class="col-md-offset-4 col-md-4 well">
            <h2><?= $title; ?></h2>
            
            <?php $error = form_error('name', "<span class='text-danger help-block'>", '</p>'); ?>    
            
            <div class="form-group <?= $error ? 'has-error' : ''; ?>">
                <label for="inputName">Name</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-user"></i>
                    </span>
                    <input type="text" class="form-control" id="inputName" name="name" placeholder="Name">
                </div>
                <?= $error; ?>                
            </div>

            <?php $error = form_error('username', "<span class='text-danger help-block'>", '</p>'); ?> 
            
            <div class="form-group <?= $error ? 'has-error' : ''; ?>">
                <label for="inputUsername">Benutzername</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-user"></i>
                    </span>                   
                    <input type="text" class="form-control" id="inputUsername" name="username" placeholder="Benutzername">
                </div>
                <?= $error; ?>   
            </div>
            
            <?php $error = form_error('email', "<span class='text-danger help-block'>", '</p>'); ?> 
            
            <div class="form-group <?= $error ? 'has-error' : '' ;?>">
                <label for="inputEmail">Email</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-envelope"></i>
                    </span> 
                    <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email">
                </div>
                <?= $error; ?>   
            </div>

            <?php $error = form_error('password', "<span class='text-danger help-block'>", '</p>'); ?> 
            
            <div class="form-group <?= $error ? 'has-error' : '';?>">                
                <label for="inputPassword">Passwort</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-lock"></i>
                    </span>                
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Passwort">
                </div>
                <?= $error; ?>   
            </div>

            <?php $error = form_error('password2', "<span class='text-danger help-block'>", '</p>'); ?>             
            
            <div class="form-group <?= $error ? 'has-error' : '';?>">
                <label for="inputPassword2">Passwort wiederholen</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-lock"></i>
                    </span>                 
                    <input type="password" class="form-control" id="inputPassword2" name="password2" placeholder="Passwort wiederholen">      
                </div>
                <?= $error; ?>   
            </div>

            <input type="submit" title="registrieren" class="btn btn-primary btn-block" value="registrieren" name="register">
        </div>
        
        <div class="col-md-4"></div>
    </div> 
<?= form_close(); ?> 