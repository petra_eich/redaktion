     <h2><?= $title; ?></h2> 

    <div class="row"> 
        <div class="col-md-12">
            <button class="btn btn-success" title="User anlegen" onclick="add_user()">
                <i class="glyphicon glyphicon-plus"></i> User anlegen
            </button>
                
            <button class="btn btn-default" title="Reload" onclick="reload_table()">
                <i class="glyphicon glyphicon-refresh"></i> Reload
            </button>
        </div>
    </div>
    <br>  
    
    <div class="row"> 
        <div class="col-md-12"> 
            <table id="userstable" class="table table-striped table-hover table-condensed table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Loginname</th>
                        <th>email</th>
                        <th>Berechtigung</th>
                        <th>registriert am</th>
                        <th class="nosort" style="min-width:200px;">Aktion</th>
                    </tr>
                </thead>

                <tbody></tbody>
                
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Loginname</th>
                        <th>email</th>
                        <th>Berechtigung</th>
                        <th>registriert am</th>  
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>        
    </div>
</div


<!-- Bootstrap modal -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Userprofil Formular</h3>
            </div>
            
            <div class="modal-body form">
                
                <form method="post" id="form" class="form-horizontal" accept-charset="utf-8" action="#" >
                    <input type="hidden" value="" name="id"> 
                    
                    <div class="form-body">           
                        <div class="form-group">
                            <label class="control-label col-md-3">Name</label>
                            
                            <div class="col-md-9">
                                <input name="name" id="name" placeholder="Name" value="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Loginname</label>
                            
                            <div class="col-md-9">
                                <input name="username" id="username" placeholder="Loginname" value="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Passwort</label>
                            
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Passwort">
                                <span class="help-block"></span>
                            </div>
                        </div>  
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            
                            <div class="col-md-9">
                                <input name="email" id="email" placeholder="Email" value="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>                      
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="role">Berechtigung</label>
                            <div class="col-md-9"> 
                                <select class="selectpicker" name="role"> 
                                    <option value="">Berechtigung</option> 
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" id="btnSave" title="Speichern" onclick="save()" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i> Speichern
                </button>
                
                <button type="button" class="btn btn-danger" title="Abbrechen" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i> Abbrechen
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="row"> <?php        
    if ( ENVIRONMENT == 'development' ) {
        
        if ( $users ) { ?>
            <pre><?php print_r($users); ?></pre> <?php 
        } 
    } ?>
</div>
