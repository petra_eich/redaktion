<?= form_open('users/login'); ?>
    <div class="row">
        <div class="col-md-offset-4 col-md-4 well">
            <h1 class="text-center"><?= $title ?></h1>
            
            <?php $error = form_error('username', "<span class='text-danger help-block'>", '</span>'); ?>
            
            <div class="form-group <?= $error ? 'has-error' : '' ;?>">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-user"></i>
                    </span> 
                    <input type="text" class="form-control" id="inputUsername" name="username" value="<?= set_value('username'); ?>" placeholder="Benutzername">
                </div>   
                <?= $error; ?>
            </div>
            
            <?php $error = form_error('password', "<span class='text-danger help-block'>", '</span>'); ?>
            
            <div class="form-group <?= $error ? 'has-error' : '' ;?>">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-lock"></i>
                    </span>   
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Passwort">                    
                </div>
                <?= $error; ?>
            </div>

            <button type="submit" title="Login" class="btn btn-primary btn-block" name="login">login</button>
        </div>
        
        <div class="col-md-4"></div>
    </div> 
<?= form_close(); ?>