<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        
        <title>Redaktion</title> <?php

        foreach ( $css as $value ) { ?>
            <link rel="stylesheet" href="<?= $value; ?>"> <?php                 
        } ?>
       
        <link rel="icon" type="image/png" href="<?= base_url() . 'favicon.png'; ?>" sizes="32x32">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() . 'apple-touch-icon.png'; ?>">
    </head>
    
    <body>
        <nav class="navbar navbar-default">
            <div class="container"> <?php
                switch ( ENVIRONMENT ) {
                    case 'development':
                        echo '<h3>LOCALE ENTWICKLUNG</h3>';
                        break;
                    case 'testing':
                        echo '<h3>TEST-SERVER</h3>';
                        break;
                    case 'allinkl':
                        echo '<h3>TEST-ALLINKL</h3>';
                        break;
                } 
                
                if ( $this->login ) { ?>
                    <div class="row userwelcome">
                        <?= $this->lang->line('text_hallo') . ' ' . strtoupper($this->loginuser) . $this->lang->line('text_anmeldung') . strtoupper($this->role); ?><br>
                    </div> <?php 
                } ?> 
                                
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>    
                    <a class="navbar-brand" href="<?= base_url(); ?>">Redaktion</a>
                </div>
                
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-left"><?php 
                        if ( $title != 'Home' ) { ?>
                            <li><a href="<?= base_url(); ?>">Home</a></li><?php 
                        } 
                        
                        if ( $this->login ) { 
                            
                            $menus = $this->menus;
                            $rolemenus = $this->rolemenus;
                            $menunames = $this->submenus;   
                            
                            $menutitle = '';

                            foreach ( $menunames as $menuname ) {

                                if ( strlen($menutitle) == 0 ) { 
                                    $menutitle = ($menuname['submenu'] == $title) ? $menuname['menu'] : '';
                                }
                            }                                 
                            
                            // MENU LEFT                        
                            foreach ( $menus as $menu ) { 
                                
                                $menu_name = $menu['name'];
                                
                                if ( $menu['section'] == 'left' && in_array($this->role, $rolemenus['left'][$menu_name]) ) { 
                                    
                                    if ( $menu['submenu'] ) {

                                        $submenus = $menu['submenu']; ?>

                                        <li class="dropdown" <?= ($menu_name == $menutitle) ? 'id="hierbinichmenu"' : ''; ?>>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <?= $menu_name; ?>
                                                <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu"> <?php

                                                foreach ( $submenus as $submenu ) {

                                                    $submenu_name = $submenu['name'];
                                                    
                                                    if ( array_key_exists($submenu_name, $rolemenus['left']) ) {
                                                        
                                                        if ( in_array($this->role, $rolemenus['left'][$submenu_name]) ) { ?>

                                                            <li <?= ($title == $submenu_name) ? 'id="hierbinich"' : ''; ?>>
                                                                <a href="<?= base_url() . $submenu['submenu_url']; ?>"><?= $submenu_name; ?></a>
                                                            </li><?php
                                                        }
                                                    }
                                                } ?>
                                            </ul>
                                        </li> <?php
                                    } else { ?>   

                                        <li <?= ($title == $menu_name) ? 'id="hierbinich"' : ''; ?>>
                                            <a href="<?= base_url() . $menu['menu_url']; ?>"><?= $menu_name; ?></a>
                                        </li> <?php 
                                    }
                                }
                            } 
                        } ?>  
                    </ul>
                                       
                    <ul class="nav navbar-nav navbar-right"><?php 
                        if ( $this->login ) {  
                            
                            if ( $this->role == 'gast' ) { 
                                
                                $this->session->set_flashdata('flash', array(
                                    'message' => 'Sorry, Deine Freischaltung dauert noch ein wenig.',
                                    'class' => 'danger',
                                ));
                            } else {
                                
                                // MENU RIGHT
                                foreach ( $menus as $menu ) { 
                                    
                                    $menu_name = $menu['name'];

                                    if ( $menu['section'] == 'right' && in_array($this->role, $rolemenus['right'][$menu_name]) ) { 
                                        
                                        if ( $menu['submenu'] ) {
                                                                                   
                                            $submenus = $menu['submenu']; ?>
                        
                                            <li class="dropdown" <?= ($menu_name == $menutitle) ? 'id="hierbinichmenu"' : ''; ?>>
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <?= $menu_name ?>
                                                    <span class="caret"></span>
                                                </a>
                                                
                                                <ul class="dropdown-menu"> <?php
                                                
                                                    foreach ( $submenus as $submenu ) {
                                                        
                                                        $submenu_name = $submenu['name'];
                                                        
                                                        if ( array_key_exists($submenu_name, $rolemenus['right']) ) {
                                                            
                                                            if ( in_array($this->role, $rolemenus['right'][$submenu_name]) ) { ?>

                                                                <li <?= ($title == $submenu_name) ? 'id="hierbinich"' : ''; ?>>
                                                                    <a href="<?= base_url() . $submenu['submenu_url']; ?>"><?= $submenu_name; ?></a>
                                                                </li><?php
                                                            }
                                                        }
                                                    } ?>
                                                </ul>
                                            </li> <?php
                                        } else { ?>
                                            
                                            <li <?= ($title == $menu_name) ? 'id="hierbinich"' : ''; ?>>
                                                <a href="<?= base_url() . $menu['menu_url']; ?>"><?= $menu_name; ?></a>
                                            </li> <?php 
                                        }
                                    }
                                } ?>
                                <li><a href="<?= base_url() . 'users/logout'; ?>">logout</a></li> <?php
                            } 
                        } else { ?>
                                
                            <li <?= ($title == 'login') ? 'id="hierbinich"' : ''; ?>>
                                <a href="<?= base_url() . 'users/login'; ?>">login</a>
                            </li>

                            <li <?= ($title == 'registrieren') ? 'id="hierbinich"' : ''; ?>>
                                <a href="<?= base_url() . 'users/register'; ?>">registrieren</a>
                            </li> <?php 
                        } ?>
                        <li <?= ($title == 'Info') ? 'id="hierbinich"' : ''; ?>>
                            <a href="<?= base_url() . 'info'; ?>">Info</a>
                        </li>
                        <li <?= ($title == 'About') ? 'id="hierbinich"' : ''; ?>>
                            <a href="<?= base_url() . 'about'; ?>">About</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container"> <?php
        
            if ($this->session->flashdata('flash')) {

                $flashmessage = $this->session->flashdata('flash'); 
                
                // nur anzeigen, wenn flash message gefüllt
                if ( strlen($flashmessage['message']) > 0) { ?>
            
                    <div id="alert" class="alert alert-dismissable fade in alert-<?= $flashmessage['class']; ?>">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?= $flashmessage['message']; ?> <?php
                        
                        // flash message leeren, damit die Meldung nicht nochmal angezeigt wird
                        $this->session->set_flashdata('flash', array('message' => '')); ?>
                    </div> <?php
                }
            } ?>
            
            <div id="lsalerttext"></div>
            