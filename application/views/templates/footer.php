    <script type="text/javascript">
        window.base_url = <?= json_encode(base_url()); ?>;
    </script> <?php
    
    foreach ( $js as $value ) { ?>
        <script type="text/javascript" charset="utf8" src="<?= $value; ?>"></script> <?php         
    } ?>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    </body>
</html>