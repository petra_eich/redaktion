<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Flash class
 *
 * This library will help to manage flash messages
 * 
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Petra Eich
 * @author          eichp <petra.eich@7days-group.com>
 * @link            http://programming-tips.in/codeigniter-manage-flash-messages/
 * @since           080717
 * 
 */


// // Set flash messages as per your need
//$this->flash->setMessage('Success message', $this->flash->getSuccessType());
//$this->flash->setMessage('Error message', $this->flash->getErrorType());
//$this->flash->setMessage('Information message', $this->flash->getInfoType());
//// Finally make all the messages flash mesages
//$this->flash->setFlashMessages();

class Flash {
 
    const INFO_MESSAGE_TYPE = 'info'; //blau
    const SUCCESS_MESSAGE_TYPE = 'success'; //grün
    const ERROR_MESSAGE_TYPE = 'danger'; //rot
    const WARNING_MESSAGE_TYPE = 'warning'; //gelb

    private $CI;

    private $flashMessages = [];

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('session');
    }

    public function getWarningType()
    {
        return self::WARNING_MESSAGE_TYPE;
    }
    
    public function getErrorType()
    {
        return self::ERROR_MESSAGE_TYPE;
    }

    public function getSuccessType()
    {
        return self::SUCCESS_MESSAGE_TYPE;
    }

    public function getInfoType()
    {
        return self::INFO_MESSAGE_TYPE;
    }

    public function setMessage($message, $messageType)
    {
        if ($messageType && $message) {
            $this->flashMessages[$messageType][] = $message;
        }
    }

    public function setFlashMessages()
    {
        $this->CI->session->set_flashdata('flash_messages', $this->flashMessages);
    }
    
}