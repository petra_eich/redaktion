<?php
class LanguageLoader {
    
    function initialize() {
        
        $ci =& get_instance();
        $ci->load->helper('language');
     
        $site_lang = $ci->session->userdata('site_lang') ? $ci->session->userdata('site_lang') : 'german';
            
        $ci->config->set_item('language', $site_lang);
        $ci->lang->load('header', $site_lang);
    }
}