<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Edit'] = 'Bearbeiten';
$lang['Delete'] = 'Löschen'; 
$lang['Add'] = 'Anlegen';
$lang['Show'] = 'Details';
$lang['Reload'] = 'Reload';
$lang['Close'] = 'Abbrechen';
$lang['Save'] = 'Speichern';


