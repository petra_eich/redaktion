<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// title
$lang['text_hallo'] = 'Hello';
$lang['text_anmeldung'] = ' - You logged in as ';

// Main Menu
$lang['text_menu_home_header'] = 'Home';
$lang['text_menu_demo_header'] = 'Live Demo'; 
$lang['text_menu_tutorials_header'] = 'Tutorials';
$lang['text_menu_lang'] = 'Languages';
$lang['text_submenu_de'] = 'German';
$lang['text_submenu_en'] = 'English';


