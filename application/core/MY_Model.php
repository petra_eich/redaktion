<?php
/**
 * this class extends MY_Model
 * 
 * my_add
 * my_count_active
 * my_count_all
 * my_count_by_lsid
 * my_count_filter
 * my_delete
 * my_delete_all
 * my_delete_by_id
 * my_get 
 * my_get_all
 * my_get_by_filter 
 * my_get_by_id 
 * my_get_id_by_name 
 * my_get_list
 * my_insert
 * my_update
 * 
 * based on http://avenir.ro/codeigniter-my_model/
 * @author  eichp   <petra_eich@web.de>
 */
class MY_Model extends CI_Model
{
    /**
     *
     * @var string
     */
    protected $table;

    /**     
     * @var array
     */
    public $where_arr = null;

    /**
     * @var string
     */
    public $select = null;

    /**
     * @var array
     */
    public $order_arr = null;

    /**
     *
     * @var integer
     */
    public $id = null;

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Insert a record into DB
     * 
     * @author  eichp           <petra.eich@7days-group.com>
     * @param   array           $insertdata     insert data
     * @return  boolean|integer insert id
     */
    public function my_add($insertdata)
    {
        if (is_array($insertdata)) {

            $this->db->insert($this->table, $insertdata);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    /**
     * Insert x records into DB
     * 
     * @param   array           $insertdata     insert data
     * @return  boolean
     */
    public function my_addbatch($insertdata)
    {
        if (is_array($insertdata)) {

            $this->db->insert_batch($this->table, $insertdata);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Count all row (acitve = 1)
     * 
     * @return  integer     number of filtred results
     */
    public function my_count_active()
    {
        $this->db->from($this->table);
        $this->db->where('active', 1);
        return $this->db->count_all_results();
    }

    /**
     * Count all rows
     * 
     * @return integer  number of all results
     */
    public function my_count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    /**
     * Count filtered rows
     *
     * @param   array   $where      where condition      
     * @return  integer number of filtred results
     */
    public function my_count_filter($where)
    {
        $this->db->from($this->table);
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    /**
     * Count filtered rows (_get_xxx_query)
     * 
     * @return integer  number of filtred results
     */
    function my_count_filtered($querystring)
    {
        switch ($querystring) {
            case 'datatables':
                $this->_get_datatables_query();
                break;
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
     * Delete row(s)
     * 
     * @param   array       $where_arr      where condition
     * @return  boolean|integer number of affected rows
     */
    public function my_delete($where_arr = null)
    {
        if (isset($where_arr)) {

            $this->db->where($where_arr);
            $this->db->delete($this->table);
            return $this->db->affected_rows();
        } else {

            return false;
        }
    }

    /**
     * Truncate table
     * 
     * @return  boolean
     */
    public function my_delete_all()
    {
        $this->db->truncate($this->table);
        return true;
    }

    /**
     * Delete row by id
     * 
     * @author  eichp       <petra.eich@7days-group.com>
     * @param   integer     $id 
     * @return  boolean|integer number of affected rows
     */
    public function my_delete_by_id($id = null)
    {
        if (isset($id)) {

            $this->db->where('id', $id);
            $this->db->delete($this->table);
            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    /**
     * Retrieve one record from DB
     * 
     * @param   array           $where_arr      where conditon
     * @return  object|boolean  row
     */
    public function my_get($where_arr = null)
    {
        if (isset($where_arr)) {

            $this->db->where($where_arr);
            $this->db->limit(1);
            $query = $this->db->get($this->table);

            if ($query->num_rows() > 0) {
                return $query->row();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retrieve records from DB
     * 
     * @param   array           $where_arr      where condition
     * @param   string          $order          order condition
     * @return  object|boolean  result
     */
    public function my_get_result($where_arr = null, $order = null)
    {
        if (isset($where_arr)) {

            $this->db->where($where_arr);

            if ($order) {

                $this->db->order_by($order);
            }
            $query = $this->db->get($this->table);

            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /** 
     * retrieve all records from DB
     * 
     * @param   boolean|array   $where_arr          where condition
     * @param   boolean|array   $order_by_var_arr   order condition
     * @param   boolean|string  $select             selected columns
     * @return  boolean|array
     */
    public function my_get_all($where_arr = null, $order_by_var_arr = null, $select = null)
    {
        if (isset($where_arr)) {
            $this->db->where($where_arr);
        }

        if (isset($order_by_var_arr)) {

            if (!is_array($order_by_var_arr)) {

                $order_by[0] = $order_by_var_arr;
                $order_by[1] = 'asc';
            } else {

                $order_by[0] = $order_by_var_arr[0];
                $order_by[1] = $order_by_var_arr[1];
            }
            $this->db->order_by($order_by[0], $order_by[1]);
        }

        if (isset($select)) {
            $this->db->select($select);
        }
        $query = $this->db->get($this->table);

        echo $this->db->last_query();

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
            return false;
        }
    }

    /**
     * Retrieve records from DB
     * 
     * @param   array           $where      where condition
     * @return  boolean|array   row
     */
    public function my_get_by_filter($where)
    {
        $this->db->from($this->table);
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        }
        return $query->row_array();
    }

    /**
     * Retrieve one record 
     * 
     * @param   integer         $id     where condition
     * @return  boolean|array   row
     */
    public function my_get_by_id($id = null)
    {
        if (isset($id)) {

            $this->db->from($this->table);
            $this->db->where('id', $id);
            $query = $this->db->get();

            return $query->row_array();
        } else {
            return false;
        }
    }

    /**
     * get id by name
     * 
     * @param   string  $name
     * @return  object  row
     */
    public function my_get_id_by_name($name)
    {

        $this->db->select('
            id
        ');
        $this->db->from($this->table);
        $this->db->where('name', $name);
        $query = $this->db->get();

        return $query->row();
    }

    /**
     * get list 
     * 
     * @param   string|boolean  $select         selected columns 
     * @param   array|boolean   $where_arr      where condition 
     * @param   array|boolean   $order_arr      order condition
     * @return  array|boolean      result array
     */
    public function my_get_list($select = null, $where_arr = null, $order_arr = null)
    {
        if ($where_arr) {
            $this->db->where($where_arr);
        }

        if (isset($order_arr)) {

            foreach ($order_arr as $order_col => $order) {
                $this->db->order_by($order_col, $order);
            }
        }
        $this->db->select($select);
        $this->db->from($this->table);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        }
        return $query->result_array();
    }

    /**
     * Insert a record into DB
     * 
     * @param   array           $columns_arr    insertdata
     * @return  boolean|integer insert id
     */
    public function my_insert($columns_arr)
    {
        if (is_array($columns_arr)) {

            if ($this->db->insert($this->table, $columns_arr)) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        }
    }

    /**
     * Update record(s)
     * 
     * @param   array           $columns_arr
     * @param   array|boolean   $where_arr
     * @return  boolean|integer number of affected rows
     */
    public function my_update($columns_arr, $where_arr = null)
    {

        if (isset($where_arr)) {

            $this->db->where($where_arr);
            $this->db->update($this->table, $columns_arr);

            if ($this->db->affected_rows() > 0) {
                return $this->db->affected_rows();
            }
        } else {

            return false;
        }
    }

}
