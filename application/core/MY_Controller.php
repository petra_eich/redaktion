<?php

class MY_Controller extends CI_Controller
{

    public $role_permissions = null;

    /**
     * @var array  config/permissions  
     */
    public $permissions = null;

    /**
     * @var array default javascripts 
     */
    protected $js = array();

    /**
     * @var array datatables javascripts 
     */
    protected $js_datatables = array();

    /**
     * @var array datepicker javascripts 
     */
    protected $js_datepicker = array();

    /**
     * @var array default css-files 
     */
    protected $css = array();

    /**
     * @var array datatables css-files 
     */
    protected $css_datatables = array();

    /**
     * @var array datepicker css-files 
     */
    protected $css_datepicker = array();

    /**
     * @var array  config/menu  
     */
    public $rolemenus = array();

    /**
     * @var array  
     */
    public $menus = array();


    /**
     * @var array    
     */
    public $submnenus = array();

    /**
     * @var boolean
     */
    public $login = false;

    /**
     * @var string username  
     */
    public $loginuser = '';

    /**
     * @var string  userrole  
     */
    public $role = '';

    /**
     * @var integer  
     */
    public $userId = '';

    /**
     * @var boolean  
     */
    public $admin = false;

    function __construct()
    {

        parent::__construct();
       
        // Get user permissions from config file
        $this->permissions = $this->config->item('user_role_permissions');
        $this->rolemenus = $this->config->item('menu');

        $this->menus = $this->menu_model->get_menulist('german');
        $this->submenus = $this->submenu_model->get_menunamen();

        $this->css = [
            base_url() . 'assets/bootstrap/css/bootstrap.min.css',
            base_url() . 'assets/lsmobil/css/style.css?v=3',
        ];

        $this->css_datatables = [
            base_url() . 'assets/datatables/css/jquery.dataTables.min.css',
            base_url() . 'assets/datatables/css/dataTables.bootstrap.min.css',
            base_url() . 'assets/datatables/css/buttons.bootstrap.min.css',
            base_url() . 'assets/datatables/css/select.dataTables.min.css',
            base_url() . 'assets/datatables/css/fixedColumns.dataTables.min.css',
        ];

        $this->css_datatablesbuttons = [
            base_url() . 'assets/datatables/css/buttons.dataTables.min.css',
        ];

        $this->css_datepicker = [
            base_url() . 'assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
        ];

        $this->css_select = [
            base_url() . 'assets/bootstrap-select/dist/css/bootstrap-select.min.css',
        ];

        $this->js = [
            base_url() . 'assets/jquery/jquery-2.1.4.min.js', // version 2.1.4 damit der Datepicker funzt
            base_url() . 'assets/bootstrap/js/bootstrap.min.js',
            base_url() . 'assets/lsmobil/js/flashmessage.js',
            base_url() . 'assets/bootstrap-fullscreen/dist/bs-modal-fullscreen.min.js',
        ];

        $this->js_datatables = [
            base_url() . 'assets/datatables/js/jquery.dataTables.min.js',
            base_url() . 'assets/datatables/js/dataTables.bootstrap.min.js',
            base_url() . 'assets/datatables/js/dataTables.buttons.min.js',
            base_url() . 'assets/datatables/js/buttons.bootstrap.min.js',
            base_url() . 'assets/datatables/js/dataTables.select.min.js',
            base_url() . 'assets/datatables/js/dataTables.fixedColumns.min.js',
        ];

        $this->js_datatablesbuttons = [
            base_url() . 'assets/datatables/js/buttons.flash.min.js',
            base_url() . 'assets/datatables/js/buttons.html5.min.js',
            base_url() . 'assets/datatables/js/buttons.print.min.js',
            base_url() . 'assets/datatables/js/buttons.colVis.min.js',
            base_url() . 'assets/datatables/js/jszip.min.js',
            base_url() . 'assets/datatables/js/pdfmake.min.js',
            base_url() . 'assets/datatables/js/vfs_fonts.js',
        ];

        $this->js_datepicker = [
            base_url() . 'assets/moment/js/moment.min.js',
            base_url() . 'assets/moment/locale/de.js',
            base_url() . 'assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        ];

        $this->js_select = [
            base_url() . 'assets/bootstrap-select/dist/js/bootstrap-select.min.js',
            base_url() . 'assets/bootstrap-select/dist/js/i18n/defaults-de_DE.min.js',
        ];

        if ($this->session->userdata('logged_in')) {

            $this->config->set_item('language', 'german');

            $this->login = true;
            $this->userId = $this->session->userdata('user_id');

            $user = $this->user_model->my_get_by_id($this->userId);

            $this->loginuser = $user['name'];
            $this->role = $this->session->userdata('role');

            if ($this->role == 'admin') {
                $this->admin = true;
            }
        }
    }

    /**
     * check user permission against config/permissions
     * 
     * @param   array   $method
     * @return  boolean
     */
    public function _check_permission($method)
    {

        return (in_array($this->role, $this->permissions[$this->router->fetch_class()][$method])) ? true : false;
    }

}
