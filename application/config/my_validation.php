<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| for validation rules
|--------------------------------------------------------------------------
|
 */

$error_msg = array(
    'required_name' => 'Bitte gib einen Namen ein.',
    'required_sequence' => 'Bitte gib eine Reihenfolgeposition ein.',
    'integer_sequence' => 'Die Reihenfolgeposition muss eine Zahl sein',
    'less_greater_sequence' => 'Bitte gib eine Reihenfolgeposition als Zahl (1 - 200) ein.',
);

$config = array(
    'beitrag_add' => array(
        array(
            'field' => 'titel',
            'label' => 'Titel',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib einen Titel ein.',
            ),
        ),
        array(
            'field' => 'meldung',
            'label' => 'Meldung',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib eine Meldung ein.',
            ),
        ),
        array(
            'field' => 'datum',
            'label' => 'Datum',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib ein Datum an.',
            ),
        ),
    ),
    'beitrag_update' => array(
        array(
            'field' => 'titel',
            'label' => 'Titel',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib einen Titel ein.',
            ),
        ),
        array(
            'field' => 'meldung',
            'label' => 'Meldung',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib eine Meldung ein.',
            ),
        ),
        array(
            'field' => 'datum',
            'label' => 'Datum',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib ein Datum an.',
            ),
        ),
        array(
            'field' => 'status_id',
            'label' => 'Bearbeitungsstatus',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib einen Bearbetungsstutus an.',
            ),
        ),
    ),
    'kategorie_add' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => array(
                'required',
                'is_unique[kategorien.name]',
            ),
            'errors' => array(
                'required' => $error_msg['required_name'],
                'is_unique' => 'Dieser {field} existiert bereits',
            ),
        ),
        array(
            'field' => 'sequence',
            'label' => 'Reihenfolge',
            'rules' => array(
                'required',
                'integer',
                'less_than[200]',
                'greater_than[0]',
            ),
            'errors' => array(
                'required' => $error_msg['required_sequence'],
                'integer' => $error_msg['integer_sequence'],
                'less_than' => $error_msg['less_greater_sequence'],
                'greater_than' => $error_msg['less_greater_sequence'],
            ),
        ),
    ),
    'kategorie_update' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => $error_msg['required_name'],
            ),
        ),
        array(
            'field' => 'sequence',
            'label' => 'Reihenfolge',
            'rules' => array(
                'required',
                'integer',
                'less_than[200]',
                'greater_than[0]',
            ),
            'errors' => array(
                'required' => $error_msg['required_sequence'],
                'integer' => $error_msg['integer_sequence'],
                'less_than' => $error_msg['less_greater_sequence'],
                'greater_than' => $error_msg['less_greater_sequence'],
            ),
        ),
    ),
    'status_add' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => array(
                'required',
                'is_unique[stati.name]',
            ),
            'errors' => array(
                'required' => $error_msg['required_name'],
                'is_unique' => 'Dieser {field} existiert bereits',
            ),
        ),
        array(
            'field' => 'sequence',
            'label' => 'Reihenfolge',
            'rules' => array(
                'required',
                'integer',
                'less_than[200]',
                'greater_than[0]',
            ),
            'errors' => array(
                'required' => $error_msg['required_sequence'],
                'integer' => $error_msg['integer_sequence'],
                'less_than' => $error_msg['less_greater_sequence'],
                'greater_than' => $error_msg['less_greater_sequence'],
            ),
        ),
    ),
    'status_update' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => $error_msg['required_name'],
            ),
        ),
        array(
            'field' => 'sequence',
            'label' => 'Reihenfolge',
            'rules' => array(
                'required',
                'integer',
                'less_than[200]',
                'greater_than[0]',
            ),
            'errors' => array(
                'required' => $error_msg['required_sequence'],
                'integer' => $error_msg['integer_sequence'],
                'less_than' => $error_msg['less_greater_sequence'],
                'greater_than' => $error_msg['less_greater_sequence'],
            ),
        ),
    ),
    'user_login' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => array(
                'required',
                'trim',
            ),
            'errors' => array(
                'required' => 'Bitte gib Deinen Usernamen ein',
                'trim' => 'Bitte prüfe Deine Eingabe.',
            ),
        ),
        array(
            'field' => 'password',
            'label' => 'Passwort',
            'rules' => 'required',
            'errors' => array(
                'required' => 'Bitte gib Dein {field} ein',
            ),
        ),
    ),
    'user_register' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => array(
                'trim',
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib Deinen Namen ein',
            ),
        ),
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => array(
                'trim',
                'required',
                'is_unique[users.username]',
            ),
            'errors' => array(
                'required' => 'Bitte gib Deinen Usernamen ein',
                'is_unique' => 'Dieser {field} existiert bereits',
            ),
        ),
        array(
            'field' => 'password',
            'label' => 'Passwort',
            'rules' => array(
                'trim',
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib Dein {field} ein',
            ),
        ),
        array(
            'field' => 'password2',
            'label' => 'Password wiederholen',
            'rules' => array(
                'trim',
                'required',
                'matches[password]',
            ),
            'errors' => array(
                'required' => 'Bitte wiederhole Dein Passwort',
                'matches' => 'Die Passwort-Eingaben stimmen nicht überein',
            ),
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => array(
                'trim',
                'required',
                'is_unique[users.email]',
                'valid_email',
            ),
            'errors' => array(
                'required' => 'Bitte gib Deine Email ein',
                'is_unique' => 'Diese Email existiert bereits',
                'valid_email' => 'Bitte gib eine gültige Email ein',
            ),
        ),
    ),
    'user_add' => array(
        array(
            'field' => 'username',
            'label' => 'Loginname',
            'rules' => array(
                'required',
                'is_unique[users.username]',
            ),
            'errors' => array(
                'required' => 'Bitte gib einen Loginnamen ein.',
                'is_unique' => 'Dieser {field} existiert bereits',
            ),
        ),
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib einen Namen ein.',
            ),
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => array(
                'required',
                'is_unique[users.email]',
            ),
            'errors' => array(
                'required' => 'Bitte gib Deine {field} ein.',
                'is_unique' => 'Diese {field} existiert bereits',
            ),
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib ein {field} ein.',
            ),
        ),
    ),
    'user_update' => array(
        array(
            'field' => 'username',
            'label' => 'Loginname',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib einen Loginnamen ein.',
            ),
        ),
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib einen Namen ein.',
            ),
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => array(
                'required',
            ),
            'errors' => array(
                'required' => 'Bitte gib Deine {field} ein.',
            ),
        ),
    ),
);