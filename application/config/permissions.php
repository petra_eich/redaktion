<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| User Role Permissions
|--------------------------------------------------------------------------
|
| Auflistung der Berechtigungen (controller/action) zu den user roles  
 */
$config['user_role_permissions'] = array(
    'beitraege' => array(
        'index' => array(
            'admin',
            'autor',
            'keyuser',
        ),
        'ajax_showupdate' => array(
            'admin',
            'autor',
            'keyuser',
        ),
        'ajax_showadd' => array(
            'admin',
            'autor',
            'keyuser',
        ),
        'ajax_add' => array(
            'admin',
            'autor',
            'keyuser',
        ),
        'ajax_update' => array(
            'admin',
            'autor',
            'keyuser',
        ),
        'ajax_delete' => array(
            'admin',
            'keyuser',
        ),
    ),
    'kategorien' => array(
        'index' => array('admin'),
        'ajax_showupdate' => array('admin'),
        'ajax_showadd' => array('admin'),
        'ajax_add' => array('admin'),
        'ajax_update' => array('admin'),
        'ajax_delete' => array('admin'),
    ),
    'stati' => array(
        'index' => array('admin'),
        'ajax_showupdate' => array('admin'),
        'ajax_showadd' => array('admin'),
        'ajax_add' => array('admin'),
        'ajax_update' => array('admin'),
        'ajax_delete' => array('admin'),
    ),
    'pages' => array(
        'index' => array(
            'admin',
            'keyuser',
            'autor',
        ),
    ),
    'users' => array(
        'index' => array(
            'admin',
            'keyuser',
        ),
        'ajax_showupdate' => array(
            'admin',
            'keyuser',
        ),
        'ajax_showadd' => array(
            'admin',
            'keyuser',
        ),
        'ajax_add' => array(
            'admin',
            'keyuser',
        ),
        'ajax_update' => array(
            'admin',
            'keyuser',
        ),
        'ajax_delete' => array(
            'admin',
        ),
    ),
);
