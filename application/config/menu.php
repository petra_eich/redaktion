<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| User Role Permissions
|--------------------------------------------------------------------------
|
| Auflistung der Berechtigungen (controller/action) zu den user roles  
| 
| menues and submenues
 */
$config['menu'] = array(   
    // LEFT    
    'left' => array(
        'Beiträge' => array(
            'admin',
            'keyuser',
            'autor',
        ),
    ),
    // RIGHT   
    'right' => array(
        'Userverwaltung' => array( // menu Stammdaten
            'admin',
            'keyuser',
        ),
        'Stammdaten' => array(
            'admin',
            'keyuser',
        ),
        'Kategorien' => array( // menu Stammdaten
            'admin',
        ),
        'Bearbeitungsstati' => array( // menu Stammdaten
            'admin',
        ),
    ),
);