var datatable = {}; 

$(document).ready(function () {

    var save_method; //for save method string
    
    $('#datepickerselect').datetimepicker({      
        locale: 'de',
        format: 'L',
    });
    
    $('#datepicker').datetimepicker({      
        locale: 'de',
        format: 'L',
        defaultDate: new Date(),
    });    
    
    datatable = $('#beitraegetable').DataTable({
        language: {
            url: './assets/datatables/locale/German.json'
        },
        // Load data for the table's content from an Ajax source
        ajax: {
            url: 'beitraege/ajax_list',
            type: 'POST'
        },
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        order: [], //Initial no order.
        columnDefs: [
            {orderable: false, targets: 'nosort'},
            {searchable: false, targets: [2, -1]}
        ]
    });

    $('#selectstatus').on('change', function () {
        
        if ( ! this.value ) {
            $('#beitraegetable').DataTable().column(3).search(this.value).draw();
        } else {
            $('#beitraegetable').DataTable().column(3).search(this.value).draw();
        }
    });
    
    // $('#selectkategorie').on('change', function () {
        
    //     if ( ! this.value ) {
    //         $('#beitraegetable').DataTable().column(2).search(this.value).draw();
    //     } else {
    //         $('#beitraegetable').DataTable().column(2).search(this.value).draw();
    //     }
    // });
    
    //change event für datetimepicker ist dp.change 
    $('#datepickerselect').datetimepicker().on('dp.change', function () {

        if ( ! $('#selectdatum').val() ) {
            $('#beitraegetable').DataTable().column(1).search($('#selectdatum').val()).draw();
        } else {
            $('#beitraegetable').DataTable().column(1).search($('#selectdatum').val()).draw();
        }
    }); 

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $('input,textarea,select').change(function () {
        
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    
    
    $('#datepicker').datetimepicker().on('dp.change', function (e) {
        
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });


    $('input[type=checkbox]').on('change', function() {

        if ( $(this).prop('checked') ) {

            $(this).val('1');
            $(this).prop('checked',true);
        } else {

            $(this).val('0');
            $(this).prop('checked',false);
        }
    });

});


function showadd_beitrag() {

    save_method = 'add';
    
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('input[type=checkbox]').val('0');
    $('input[type=checkbox]').attr('checked',false);

    $('#lsalerttext').empty;
    $('.lsalert').remove();
        
    //Ajax Load data from ajax
    $.ajax({
        url: 'beitraege/ajax_show' + save_method,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            
            if ( data.login == false ) {
                document.location.href = 'users/login';
            }
            
            if ( data.permission == false ) {
                
                $('#lsalerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>'); 
                window.setTimeout(function() {
                        $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                        $(this).remove(); 
                    });
                }, 7000);
            } else {    
                
                $('.statushidden').toggle(false);
                $('select[name="status_id"]').empty();  
                
                $('.selectpicker').selectpicker('refresh');
                $('#modal_form').modal('show');  
                $('.modal-title').text('Beitrag anlegen'); 
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ajaxfehler: Daten können nicht erzeugt werden');
        }
    });    
}


function showupdate_beitrag(id) {
    
    save_method = 'update';
    
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('input[type=checkbox]').val('0');
    $('input[type=checkbox]').attr('checked',false);
    
    $('#lsalerttext').empty;
    $('.lsalert').remove();   

    //Ajax Load data from ajax
    $.ajax({
        url: 'beitraege/ajax_show' + save_method + '/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            
            if ( data.login == false ) {
                document.location.href = 'users/login';
            }
            
            if ( data.permission == false ) {
                
                $('#lsalerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>'); 
                window.setTimeout(function() {
                        $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                        $(this).remove(); 
                    });
                }, 7000);
            } else {    
                var beitrag = data.beitrag;
                $('#modal_form').modal('show');

                $('[name="id"]').val(beitrag.id);
                $('[name="titel"]').val(beitrag.titel);
                $('[name="meldung"]').val(beitrag.meldung);
                $('[name="datum"]').val(beitrag.datum); 
                $('.statushidden').toggle(data.admin);
                $('select[name="status_id"]').empty();
                $.each(data.stati, function (key, value) {
                    $('select[name="status_id"]').append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                $('.selectpicker').selectpicker('refresh');

                $.each(data.kategorien, function (key, value) {

                    if (value.beitrag == 1) {

                        $('#check_' + value.id).val('1');
                        $('#check_' + value.id).prop('checked',true);
                    }
                });
                $('.modal-title').text('Beitrag bearbeiten'); 
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ajaxfehler: Daten können nicht erzeugt werden');
        }
    });
}


function reload_table() {
    datatable.ajax.reload(null, false);
}


function save() {
    
    $('#btnSave').text('speichern...'); 
    $('#btnSave').attr('disabled', true); 
    
    $('#beitraegealerttext').empty;
    $('#lsalerttext').empty;
    $('.lsalert').remove(); 
    
    var url = 'beitraege/ajax_' + save_method;

    // ajax adding data to database
    var formData = new FormData($('#form')[0]);
    
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
            
            if ( data.login == false ) {
                document.location.href = 'users/login';
            }

            if ( data.permission == false ) {
                
                $('#beitraegealerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>'); 
                window.setTimeout(function() {
                        $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                        $(this).remove(); 
                    });
                }, 7000);
            } else {
                //if success close modal and reload ajax table
                if ( data.success ) {
                    
                    $('#modal_form').modal('hide');
                    reload_table();
                } else {
                    
                    console.log('validation error');

                    $.each(data.messages, function (key, value) {
                        
                        if ( $('[name="' + key + '"]').hasClass('selectpicker') || $('[name="' + key + '"]').hasClass('datepicker' )) {

                            $('[name="' + key + '"]').parent().parent().parent().addClass('has-error'); 
                            $('[name="' + key + '"]').parent().next().html(value);
                        } else {

                            $('[name="' + key + '"]').parent().parent().addClass('has-error'); 
                            $('[name="' + key + '"]').next().html(value);
                        }
                    });
                }
            }
            $('#btnSave').html('<i class="glyphicon glyphicon-ok"></i> Speichern'); 
            $('#btnSave').attr('disabled', false); 
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Fehler beim Anlegen / Bearbeiten des Datensatzes');
            $('#btnSave').html('<i class="glyphicon glyphicon-ok"></i> Speichern'); 
            $('#btnSave').attr('disabled', false); 
        }
    });
}


function delete_beitrag(id) {
    
    $('#lsalerttext').empty;
    $('.lsalert').remove();
    
    if ( confirm('Soll die Beitrag wirklich gelöscht werden?') ) {
        
        // ajax delete data to database
        $.ajax({
            url: 'beitraege/ajax_delete/' + id,
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                
                if ( data.login == false ) {
                    document.location.href = 'users/login';
                }
                
                if ( data.permission == false ) {
                    
                    $('#lsalerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>');
                    window.setTimeout(function() {
                            $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                            $(this).remove(); 
                        });
                    }, 7000);
                } else {
                    
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Fehler beim Löschvorgang');
            }
        });
    }
}
