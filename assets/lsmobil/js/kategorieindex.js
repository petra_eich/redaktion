var datatable = {}; //for save method string

$(document).ready(function () {

    var save_method; //for save method string
    
    datatable = $('#kategorientable').DataTable({
        language: {
            url: './assets/datatables/locale/German.json'
        },
        // Load data for the table's content from an Ajax source
        ajax: {
            url: 'kategorien/ajax_list',
            type: 'POST'
        },
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        order: [], //Initial no order.
        columnDefs: [
            {orderable: false, targets: 'nosort'},
            {searchable: false, targets: [-1]}
        ]
    });


    //set input/select event when change value, remove class error and remove text help block 
    $('input,select').change(function () {
        
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    
    $('#inputActive').change(function(){

        if ( $(this).prop('checked') ) {
            
            $(this).val('1');
            $(this).attr('checked',true);
        } else {
            
            $(this).val('0');
            $(this).attr('checked',false);
        }
    });
});


function add_kategorie() {

    save_method = 'add';
    
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    
    $('#lsalerttext').empty;
    $('.lsalert').remove();    
        
    //Ajax Load data from ajax
    $.ajax({
        url: 'kategorien/ajax_show' + save_method,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            if (data.login == false) {
                document.location.href = 'users/login';
            }
            
            if ( data.permission == false ) {
                
                $('#lsalerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>'); 
                window.setTimeout(function() {
                        $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                        $(this).remove(); 
                    });
                }, 7000);
            } else {    

                $('#modal_form').modal('show'); // show bootstrap modal  
                $('.modal-title').text('Kategorie anlegen'); // Set Title to Bootstrap modal title
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ajaxfehler: Daten können nicht erzeugt werden');
        }
    });    
}


function edit_kategorie(id) {
    
    save_method = 'update';
    
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    
    $('#lsalerttext').empty;
    $('.lsalert').remove();   

    //Ajax Load data from ajax
    $.ajax({
        url: 'kategorien/ajax_show' + save_method + '/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            if (data.login == false) {
                document.location.href = 'users/login';
            }
            
            if ( data.permission == false ) {
                
                $('#lsalerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>'); 
                window.setTimeout(function() {
                        $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                        $(this).remove(); 
                    });
                }, 7000);
            } else {    
                
                $('[name="id"]').val(data.id);
                $('[name="name"]').val(data.name);
                $('[name="sequence"]').val(data.sequence);
                $('[name="active"]').val(data.active);
                $('#modal_form').modal('show'); // show bootstrap modal 
                $('.modal-title').text('Kategorie bearbeiten'); // Set title to Bootstrap modal title
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ajaxfehler: Daten können nicht erzeugt werden');
        }
    });
}


function reload_table() {
    datatable.ajax.reload(null, false);
}


function save() {
    
    $('#btnSave').text('speichern...'); //change button text
    $('#btnSave').attr('disabled', true); //set button disable 
    
    $('#kategorienalerttext').empty;
    $('#lsalerttext').empty;
    $('.lsalert').remove(); 
    
    var url = 'kategorien/ajax_' + save_method;

    // ajax adding data to database
    var formData = new FormData($('#form')[0]);
    
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
            if (data.login == false) {                
                document.location.href = 'users/login';
            }

            if ( data.permission == false ) {
                
                $('#kategorienalerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>'); 
                window.setTimeout(function() {
                        $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                        $(this).remove(); 
                    });
                }, 7000);
            } else {     
                
                //if success close modal and reload ajax table
                if (data.success) {
                    
                    $('#modal_form').modal('hide');
                    reload_table();
                } else {
                    
                    console.log('validation error');

                    $.each(data.messages, function (key, value) {
                        if ($('[name="' + key + '"]').hasClass('selectpicker')){

                            $('[name="' + key + '"]').parent().parent().parent().addClass('has-error'); 
                            $('[name="' + key + '"]').parent().next().html(value);
                        } else {

                            $('[name="' + key + '"]').parent().parent().addClass('has-error'); 
                            $('[name="' + key + '"]').next().html(value);
                        }
                    });
                }
            }
            $('#btnSave').html('<i class="glyphicon glyphicon-ok"></i> Speichern');
            $('#btnSave').attr('disabled', false); 
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Fehler beim Anlegen / Bearbeiten des Datensatzes');
            $('#btnSave').html('<i class="glyphicon glyphicon-ok"></i> Speichern');
            $('#btnSave').attr('disabled', false); 
        }
    });
}


function delete_kategorie(id) {
    
    $('#lsalerttext').empty;
    $('.lsalert').remove();
    
    if (confirm('Soll das Kategorie wirklich gelöscht werden?')) {
        
        // ajax delete data to database
        $.ajax({
            url: 'kategorien/ajax_delete/' + id,
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                if (data.login == false) {
                    document.location.href = 'users/login';
                }
                
                if ( data.permission == false ) {
                    
                    $('#lsalerttext').html('<div class="alert lsalert alert-dismissable fade in alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Berechtigung fehlt!</div>');
                    window.setTimeout(function() {
                            $('.lsalert').fadeTo(1000, 0).slideUp(1000, function(){
                            $(this).remove(); 
                        });
                    }, 7000);
                } else {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Fehler beim Löschvorgang');
            }
        });
    }
}
