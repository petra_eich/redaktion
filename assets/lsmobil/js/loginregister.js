$(document).ready(function () {
    
    //set input/select event when change value, remove class error and remove text help block 
    $('input,select').change(function () {

        $(this).parent().parent().removeClass('has-error');
        $(this).parent().next().empty();
    });

});