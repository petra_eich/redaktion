-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 11. Feb 2019 um 10:09
-- Server-Version: 10.1.37-MariaDB
-- PHP-Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `redaktion`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beitraege`
--

CREATE TABLE `beitraege` (
  `id` int(11) NOT NULL COMMENT 'in Tabelle beitragkategorien',
  `titel` varchar(200) NOT NULL,
  `meldung` text NOT NULL,
  `status_id` int(11) NOT NULL COMMENT 'aus Tabelle stati',
  `autor_id` int(11) NOT NULL COMMENT 'aus Tabbelle users (autor)',
  `datum` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `beitraege`
--

INSERT INTO `beitraege` (`id`, `titel`, `meldung`, `status_id`, `autor_id`, `datum`, `created`, `modified`) VALUES
(1, 'test', 'blub', 2, 0, '2019-02-08', '2019-02-07 11:27:00', '2019-02-10 18:18:16'),
(2, 'bla', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et e', 4, 179, '2019-02-06', '2019-02-07 19:03:10', '2019-02-11 09:06:51'),
(12, 'Neuer Beitrag', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea tak', 1, 0, '2019-02-10', '2019-02-10 14:09:07', '2019-02-11 09:04:24'),
(14, 'noch ein Beitrag', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 1, 0, '2019-02-10', '2019-02-10 16:46:27', '2019-02-11 09:08:43');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beitragkategorien`
--

CREATE TABLE `beitragkategorien` (
  `id` int(11) NOT NULL,
  `beitrag_id` int(11) NOT NULL COMMENT 'aus Tabelle beitraege ',
  `kategorie_id` int(11) NOT NULL COMMENT 'aus Tabelle kategorien',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `beitragkategorien`
--

INSERT INTO `beitragkategorien` (`id`, `beitrag_id`, `kategorie_id`, `created`, `modified`) VALUES
(19, 14, 1, '2019-02-10 16:46:27', '2019-02-10 16:46:27'),
(20, 14, 3, '2019-02-10 16:46:27', '2019-02-10 16:46:27'),
(23, 12, 1, '2019-02-10 17:19:56', '2019-02-10 17:19:56'),
(24, 12, 2, '2019-02-10 17:19:56', '2019-02-10 17:19:56'),
(25, 12, 3, '2019-02-10 17:19:56', '2019-02-10 17:19:56');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kategorien`
--

CREATE TABLE `kategorien` (
  `id` int(11) NOT NULL COMMENT 'in Tabelle beitragkategorien',
  `name` varchar(30) NOT NULL,
  `sequence` int(3) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `kategorien`
--

INSERT INTO `kategorien` (`id`, `name`, `sequence`, `active`, `created`, `modified`) VALUES
(1, 'cat 1', 2, 1, '2019-02-07 12:09:28', '2019-02-08 15:35:23'),
(2, 'cat 2', 0, 1, '2019-02-07 12:19:17', '2019-02-07 12:19:17'),
(3, 'cat', 66, 1, '2019-02-09 18:23:32', '2019-02-09 18:23:32');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `menu_url` varchar(30) NOT NULL,
  `section` varchar(15) NOT NULL,
  `sequence` int(3) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(2) NOT NULL DEFAULT 'de',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `menus`
--

INSERT INTO `menus` (`id`, `name`, `menu_url`, `section`, `sequence`, `active`, `lang`, `created`, `modified`) VALUES
(8, 'Stammdaten', '', 'right', 80, 1, 'de', '2018-07-13 07:06:32', '2018-07-16 06:10:53'),
(20, 'Beiträge', 'beitraege', 'left', 80, 1, 'de', '2018-07-12 13:23:53', '2019-02-07 16:09:42');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stati`
--

CREATE TABLE `stati` (
  `id` int(11) NOT NULL COMMENT 'in Tabelle beitraege',
  `name` varchar(30) NOT NULL,
  `sequence` int(3) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `stati`
--

INSERT INTO `stati` (`id`, `name`, `sequence`, `active`, `created`, `modified`) VALUES
(1, 'offen', 3, 1, '2017-10-23 10:03:23', '2019-02-07 11:31:39'),
(2, 'erledigt', 2, 1, '2017-10-23 10:04:11', '2019-02-08 15:35:36'),
(4, 'inaktiv', 3, 1, '2017-10-23 10:04:33', '2019-02-07 11:29:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `submenus`
--

CREATE TABLE `submenus` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL COMMENT 'aus tabelle menus',
  `name` varchar(30) NOT NULL,
  `submenu_url` varchar(30) NOT NULL,
  `sequence` int(3) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `submenus`
--

INSERT INTO `submenus` (`id`, `menu_id`, `name`, `submenu_url`, `sequence`, `active`, `created`, `modified`) VALUES
(1, 8, 'Kategorien', 'kategorien', 90, 1, '2018-07-13 08:23:57', '2019-02-07 12:08:50'),
(2, 8, 'Bearbeitungsstati', 'stati', 80, 1, '2018-07-13 08:23:57', '2019-02-07 10:49:24'),
(10, 8, 'Userverwaltung', 'users', 99, 1, '2018-07-13 08:26:52', '2018-07-16 07:10:20');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'in Tabellen beitraege',
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT 'keine Mail',
  `role` varchar(15) NOT NULL DEFAULT 'gast' COMMENT 'admin, keyuser, autor, gast',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `role`, `created`) VALUES
(8, 'Petra', 'petra', '$2y$10$jAPg7FxKXaSM/mOxpGExle.O6zl48vKaQ4eOA7msyr1S79MsoMKyq', 'petra_eich@web.de', 'admin', '2017-09-06 11:54:48'),
(178, 'keyuser', 'keyuser', '$2y$10$mDLCXRBB7ri8g3c8ImAMvefeHPAOiG62KCF/1F0GOxjIOWp41xA8a', 'keyuser@test.de', 'keyuser', '2019-01-18 10:29:08'),
(179, 'Autor', 'autor', '$2y$10$i8Wp5cGLTv8Jw9lGHtzAduq1/W18h6w8qfGqhNVJt01nNMyJ7z8O.', 'autor@test.de', 'autor', '2019-01-18 10:33:06'),
(182, 'Admin', 'admin', '$2y$10$3vqnyBtKgFZ1ck3NLrTKLuvAOMDnKOCpckvDQWR70H3Jvmxb21KTS', 'admin@test.de', 'admin', '2019-01-18 10:37:57'),
(183, 'autor1', 'autor1', '$2y$10$lOJFsV9/6llUtKuExAi6g.5FxuFV8fHutgNh2qOJQK8qiMu7GYxZ2', 'autor1@test.de', 'autor', '2019-02-08 17:44:04');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `beitraege`
--
ALTER TABLE `beitraege`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `titel` (`titel`,`datum`) USING BTREE;

--
-- Indizes für die Tabelle `beitragkategorien`
--
ALTER TABLE `beitragkategorien`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `beitragkategorie` (`beitrag_id`,`kategorie_id`);

--
-- Indizes für die Tabelle `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indizes für die Tabelle `kategorien`
--
ALTER TABLE `kategorien`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indizes für die Tabelle `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indizes für die Tabelle `stati`
--
ALTER TABLE `stati`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indizes für die Tabelle `submenus`
--
ALTER TABLE `submenus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `user` (`name`,`username`),
  ADD KEY `name` (`name`) USING BTREE;

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `beitraege`
--
ALTER TABLE `beitraege`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'in Tabelle beitragkategorien', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT für Tabelle `beitragkategorien`
--
ALTER TABLE `beitragkategorien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT für Tabelle `kategorien`
--
ALTER TABLE `kategorien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'in Tabelle beitragkategorien', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT für Tabelle `stati`
--
ALTER TABLE `stati`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'in Tabelle beitraege', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `submenus`
--
ALTER TABLE `submenus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'in Tabellen beitraege', AUTO_INCREMENT=184;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
